﻿using UnityEngine;
using System.Collections;

public class Lightflash : MonoBehaviour {

	public GameObject _Light1;
	public GameObject _Light2;
	public GameObject _Light3;
	public GameObject _Light4;
	public GameObject _Light5;
	public GameObject _Light6;

	public float timer;

	public int _timer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		_timer = (int)timer % 2;
		if (_timer == 1) 
		{
			//Debug.Log("Flash every second");
			_Light1.GetComponent<Light>().intensity=8;
			_Light2.GetComponent<Light>().intensity=8;
			_Light3.GetComponent<Light>().intensity=8;
			_Light4.GetComponent<Light>().intensity=8;
			_Light5.GetComponent<Light>().intensity=8;
			_Light6.GetComponent<Light>().intensity=8;
//			_Light1.SetActive(true);
//			_Light2.SetActive(true);
//			_Light3.SetActive(true);
//			_Light4.SetActive(true);
//			_Light5.SetActive(true);
//			_Light6.SetActive(true);

		}

		if (_timer == 0) 
		{
			_Light1.GetComponent<Light>().intensity=0;
			_Light2.GetComponent<Light>().intensity=0;
			_Light3.GetComponent<Light>().intensity=0;
			_Light4.GetComponent<Light>().intensity=0;
			_Light5.GetComponent<Light>().intensity=0;
			_Light6.GetComponent<Light>().intensity=0;

//			_Light1.SetActive(false);
//			_Light2.SetActive(false);
//			_Light3.SetActive(false);
//			_Light4.SetActive(false);
//			_Light5.SetActive(false);
//			_Light6.SetActive(false);
			//Debug.Log("Flash every 0 Second");
		}
	}
}
