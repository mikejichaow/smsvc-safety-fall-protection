Shader "UnlitAlpha"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _adjust ("adjust vertex color", Color) = (1.0,1.0,1.0,1.0)
       // _MainTex ("Base (RGB) Trans. (Alpha)", 2D) = "white" { }
    }

    Category
    {
        ZWrite On
        Alphatest Greater 0.5
        Cull Off
        SubShader
        {
            Blend SrcAlpha OneMinusSrcAlpha
            Tags { "Queue"="Transparent" "RenderType"="Transparent" }
            Pass
            {
                Lighting Off

            }
             pass
        {
            CGPROGRAM
            #pragma vertex wfiVertCol
            #pragma fragment passThrough
            #include "UnityCG.cginc"
            
            uniform float4 _adjust;
            struct VertOut
            {
                float4 position : POSITION;
                float4 color : COLOR;
            };

            struct VertIn
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
            };

            VertOut wfiVertCol(VertIn input, float3 normal : NORMAL)
            {
                VertOut output;
                output.position = mul(UNITY_MATRIX_MVP,input.vertex);
                output.color = input.color ;
                return output;
            }

            struct FragOut
            {
                float4 color : COLOR;
            };

            FragOut passThrough(float4 color : COLOR)
            {
                FragOut output;
                output.color = color * _adjust;
                return output;
            }
            ENDCG

        }
        //FallBack "Diffuse"
        } 
    }
}