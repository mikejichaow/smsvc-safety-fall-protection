    Shader "Vertex Colored Alpha 0.5" {
        Properties {
            //adjustalpha ("Main Color", Color) = (1,1,1,0.5)
            MainTex ("Base (RGB)", 2D) = "white" {}
        }
       Category
    {
        ZWrite On
        Alphatest Greater 0.5
        Cull Off
        SubShader {
            Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
            LOD 100
            Alphatest Greater 0
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma surface surf Lambert alpha
            //lighting off
            sampler2D _MainTex;
            uniform float4 adjustalpha;
            struct Input {
                float2 uv_MainTex;
                float4 color: Color; // Vertex color
                float4 adjustalpha;
            };
     
            void surf (Input IN, inout SurfaceOutput o) {
                half4 c = tex2D (_MainTex, IN.uv_MainTex);
                o.Albedo = c.rgb * IN.color.rgb; // vertex RGB
                o.Alpha = c.a * IN.color.a *8 ; // vertex Alpha
            }

            ENDCG
        }
        FallBack "Diffuse"
       }
    }