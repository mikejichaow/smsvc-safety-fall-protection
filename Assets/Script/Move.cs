﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public float speed;
	// Use this for initialization
	void Start () {
		speed = 5;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W))
		{


			transform.position -= Vector3.left * speed * (Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.S))
		{
			transform.position += Vector3.left * speed * (Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.E))
		{
			transform.position += Vector3.up * speed * (Time.deltaTime);

		}
		if (Input.GetKey(KeyCode.C))
		{
			transform.position -= Vector3.up * speed * (Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.A))
		{
			transform.position += Vector3.forward * speed * (Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.position -= Vector3.forward * speed * (Time.deltaTime);
		}


	}
}
