﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InventorySystem : MonoBehaviour {
//
//
	public GameObject Tool1;
	public GameObject Tool2;
	public GameObject Tool3;
	public GameObject Tool4;

	//public GameObject Character;
	private RaycastHit Hit;
	public GameObject PickupPanel;
	public Text ToolDescription;

	public GameObject door;
	private Animator dooranimation;
	public GameObject Doorwarning;

	public GameObject startworkbutton;
	public GameObject MessagePanel;

	public Sprite _Tool1;
	public Sprite _Tool2;
	public Sprite _Tool3;
	public Sprite _Tool4;

	public GameObject ToolImage;

	public GameObject ItemImage1;
	public GameObject ItemImage2;
	public GameObject ItemImage3;
	public GameObject ItemImage4;
	public GameObject ItemImage5;
	public GameObject ItemImage6;

	public bool Btool1;
	public bool Btool2;
	public bool Btool3;
	public bool Btool4;

	public bool Usetool1;
	public bool Usetool2;
	public bool Usetool3;
	public bool Usetool4;

	public int a;
	public int b;
	public int c;
	public int d;


	public GameObject BeamClamp1;
	public GameObject BeamClamp2;
	public Vector3 positionA ;
	public Quaternion rotationA;

	public GameObject Lock;
	public GameObject point;

	public bool HookMode;
	public  GameObject InstructionPanel;
	public  Text InstructionText;
	public static bool attach1;
	public static bool attach2;
	public static bool release1;
	public static bool release2;
	public bool tightenBC;




	public GameObject Book;
	public GameObject BookPanel;

	// Use this for initialization
	void Start () {
		//==========================================================================================================Debug mode==================================================
		HookMode = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Ray ray = new Ray (transform.position, transform.rotation);
//		RaycastHit hit;
//		if (Input.GetMouseButtonDown (0)) 
//		{
//
//		}

//		Vector3 myTransform = Character.transform.forward;
//		if (Input.GetKey (KeyCode.Mouse0)) 
//		{
//			if(Physics.Raycast(transform.position,myTransform,out Hit,100))
//			{
//				if(Hit.collider.gameObject.tag=="Tool1")
//				{
//					Debug.Log("Hit tool1");
//				}
//
//				if(Hit.collider.gameObject.tag=="Tool2")
//				{
//					Debug.Log("Hit tool2");
//				}
//
//				if(Hit.collider.gameObject.tag=="Tool3")
//				{
//					Debug.Log("Hit tool3");
//				}
//
//				if(Hit.collider.gameObject.tag=="Tool4")
//				{
//					Debug.Log("Hit tool4");
//				}
//			}
//		}


		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if(Input.GetMouseButtonDown(0))
		{


			//Debug.Log("a= " + a);
			if (Physics.Raycast (ray,out hit, 1000)) 
			{
				if(hit.transform.tag == "Book")
				{

					Debug.Log("Book");
					Book.SetActive(false);
					BookPanel.SetActive(true);

				}

				if(hit.transform.tag == "Tool1")
				{
					Debug.Log("Hit Tool1");
					PickupPanel.SetActive(true);
					ToolImage.GetComponent<Image>().sprite=_Tool1;
					Tool1.SetActive(false);
					Btool1=true;
					Btool2=false;
					Btool3=false;
					Btool4=false;



//					if (ItemImage1.GetComponent<Image> ().sprite == null)
//					{
//						a=1;
//						
//					} 
//					else if(ItemImage2.GetComponent<Image> ().sprite== null)
//					{
//						a=2;
//					}
//					else if(ItemImage3.GetComponent<Image> ().sprite == null)
//					{
//						a=3;
//					}
//					
//					else if(ItemImage4.GetComponent<Image> ().sprite == null)
//					{
//						a=4;
//					}


				}

				if(hit.transform.tag == "Tool2")
				{
					Debug.Log("Hit Tool2");
					PickupPanel.SetActive(true);
					ToolImage.GetComponent<Image>().sprite=_Tool2;
					Tool2.SetActive(false);
					Btool1=false;
					Btool2=true;
					Btool3=false;
					Btool4=false;

					ItemImage2.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
					ItemImage2.SetActive(true);
					//					if (ItemImage1.GetComponent<Image> ().sprite == null)
//					{
//						b=1;
//						
//					} 
//					else if(ItemImage2.GetComponent<Image> ().sprite == null)
//					{
//						b=2;
//					}
//					else if(ItemImage3.GetComponent<Image> ().sprite == null)
//					{
//						b=3;
//					}
//					
//					else if(ItemImage4.GetComponent<Image> ().sprite == null)
//					{
//						b=4;
//					}
				}

				if(hit.transform.tag == "Tool3")
				{
					Debug.Log("Hit Tool3");
					PickupPanel.SetActive(true);
					ToolImage.GetComponent<Image>().sprite=_Tool3;
					Tool3.SetActive(false);
					Btool1=false;
					Btool2=false;
					Btool3=true;
					Btool4=false;
					ItemImage3.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
					ItemImage3.SetActive(true);

					//					if (ItemImage1.GetComponent<Image> ().sprite == null)
//					{
//						c=1;
//						
//					} 
//					else if(ItemImage2.GetComponent<Image> ().sprite == null)
//					{
//						c=2;
//					}
//					else if(ItemImage3.GetComponent<Image> ().sprite == null)
//					{
//						c=3;
//					}
//					
//					else if(ItemImage4.GetComponent<Image> ().sprite == null)
//					{
//						c=4;
//					}
				}

				if(hit.transform.tag == "Tool4")
				{
					Debug.Log("Hit Tool4");
					PickupPanel.SetActive(true);
					ToolImage.GetComponent<Image>().sprite=_Tool4;
					Tool4.SetActive(false);
					Btool1=false;
					Btool2=false;
					Btool3=false;
					Btool4=true;
					ItemImage4.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
					ItemImage4.SetActive(true);
					//					if (ItemImage1.GetComponent<Image> ().sprite == null)
//					{
//						d=1;
//						
//					} 
//					else if(ItemImage2.GetComponent<Image> ().sprite == null)
//					{
//						d=2;
//					}
//					else if(ItemImage3.GetComponent<Image> ().sprite == null)
//					{
//						d=3;
//					}
//				
//					else if(ItemImage4.GetComponent<Image> ().sprite == null)
//					{
//						d=4;
//					}
				}
//				Debug.Log("a= " + a);
//				Debug.Log("b= " + b);
//				Debug.Log("c= " + c);
//				Debug.Log("d= " + d);
//			

				if(hit.transform.tag=="BeamClamp")
				{
					BeamClamp1.SetActive(false);
					BeamClamp2.SetActive(true);

					//RopeScale.Beamclamplight.SetActive(false);
					InstructionPanel.SetActive(true);
					InstructionText.text="Beamclamp has attached to the beam, click to tighten it.";
				}

				if(hit.transform.tag=="Point")
				{
					Lock.transform.Translate(-0.7f,0,0);
					point.SetActive(false);

					InstructionPanel.SetActive(true);
					tightenBC=true;
					InstructionText.text="Click the beamclamp anchor point.";
				}

			}

			//===========================================================================Working mode========================================================================================



			//======================================================================================================================================================================
		}


	}
	public void releasehook()
	{
		release1 = true;
		attach1=false;
	}

	public void SetActive (GameObject g)
	{
		if (g.activeSelf == true) {
			g.SetActive (false);
		} else {
			g.SetActive (true);
		}
		
	}


	public void pickupitems()
	{
		//Debug.Log("Item Picked");
		//ItemImage1.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
		//ItemImage1.SetActive(true);
		//ToolDescription.text="";
		if (Btool1) 
		{

		}

		if (Btool2) 
		{
			
		}

		if (Btool3) 
		{
			
		}

		if (Btool4) 
		{
			
		}

//		if (ItemImage1.GetComponent<Image> ().sprite == null)
//		{
//			ItemImage1.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage1.SetActive(true);
//
//		} 
//		else if(ItemImage2.GetComponent<Image> ().sprite == null)
//		{
//			ItemImage2.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage2.SetActive(true);
//		}
//		else if(ItemImage3.GetComponent<Image> ().sprite == null)
//		{
//			ItemImage3.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage3.SetActive(true);
//		}
//
//		else if(ItemImage4.GetComponent<Image> ().sprite == null)
//		{
//			ItemImage4.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage4.SetActive(true);
//		}
//
//		else if(ItemImage5.GetComponent<Image> ().sprite == null)
//		{
//			ItemImage5.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage5.SetActive(true);
//		}
//
//		else if(ItemImage6.GetComponent<Image> ().sprite ==null)
//		{
//			ItemImage6.GetComponent<Image> ().sprite=ToolImage.GetComponent<Image>().sprite;
//			ItemImage6.SetActive(true);
//		}

	}

	public void checklabel()
	{
		if (!Tool1.activeSelf&&Btool1)
		{
			ToolDescription.text="This is Beam Clamp \n the length:\n the width is: ";
		}
		
		if (!Tool2.activeSelf&&Btool2)
		{
			ToolDescription.text="This is Beamstrap \n the length:\n the width is: ";
		}
		
		if (!Tool3.activeSelf&&Btool3)
		{
			ToolDescription.text="This is Self lifeline \n the length:\n the width is: ";
		}
		
		if (!Tool4.activeSelf&&Btool4)
		{
			ToolDescription.text="This is tool-4 \n the length:\n the width is: ";
		}
	}


	public void putitdown()
	{
		if (Btool2) 
		{
			Tool2.SetActive(true);
			Btool2=false;
		}

		if (Btool1) 
		{
			Debug.Log("Put item1 down");
			Tool1.SetActive(true);
			Btool1=false;
		}

		if (Btool3) 
		{
			Debug.Log("Put item3 down");
			Tool3.SetActive(true);
			Btool3=false;
		}

		if (Btool4) 
		{
			Tool4.SetActive(true);
			Btool4=false;
		}

	}
	public void put1itemdown()
	{
	
			ItemImage1.GetComponent<Image> ().sprite = null;
			ItemImage1.SetActive(false);
			Tool1.SetActive(true);
			Btool1=false;
			ToolDescription.text="";

	}
//
	public void put2itemdown()
	{
			ItemImage2.GetComponent<Image> ().sprite = null;
			ItemImage2.SetActive(false);
			Tool2.SetActive(true);
			Btool2=false;
			ToolDescription.text="";
	}
//
	public void put3itemdown()
	{

			ItemImage3.GetComponent<Image> ().sprite = null;
			ItemImage3.SetActive(false);
			Tool3.SetActive(true);
			Btool3=false;
			ToolDescription.text="";	
		
	}
//
	public void put4itemdown()
	{
			ItemImage4.GetComponent<Image> ().sprite = null;
			ItemImage4.SetActive(false);
			Tool4.SetActive(true);
			Btool4=false;
			ToolDescription.text="";

	}



	public void Item1Used()
	{

		Usetool1 = true;

	}
//
	public void Item2Used()
	{

			Usetool2 = true;

	}

	public void Item3Used()
	{

			Usetool3 = true;
	}
//
	public void Item4Used()
	{

			Usetool4 = true;

	}

	public void checktools()
	{
		//if ( Usetool2 && Usetool3) {

		MessagePanel.SetActive (true);
		MessagePanel.GetComponent<Text>().text="Click the 'Inventory' button to check all the tools you selected. ";
		

	}

//	public void opendoor()
//	{
//		if (attach1) {
//			dooranimation = door.GetComponent<Animator> ();
//			dooranimation.enabled = true;
//		} else
//			Doorwarning.SetActive (true);
//	}
//================================================================================================================================================================





}



//	public const int TOTAL =11;
//	public GameObject[] Tool;
//	public Sprite[] ItemImage = new Sprite[TOTAL];
//	public GameObject[] slots = new GameObject[TOTAL];
//	public GameObject PickupPanel;
//	public GameObject ToolImage;
//	private RaycastHit Hit;
//	public int currentI;
//	public int c = 0;
//	public List<bool> slotList;
//
//	public GameObject[] test;
//	public int[] i1;
//
//	public bool eyeCheckB;
//	public bool earCheckB;
//	
//	public GameObject msgDisplay;
//	bool msgB;
//	float msgTimer;
//	string msgText;
//	
//	//public InventorySlot slot;
//	
//	
//	void Start(){
//
//	}
//	
//	public void PickUpItems(){
//		
//		if (slotList.Contains (false)) {
//			c = slotList.IndexOf(false);
//		}else{
//			c = slotList.Count;
//			slotList.Add (true);
//		}
//		//ItemImage [c].SetActive (true);
//		Debug.Log ("c= " +c);
//		slots[c] = new GameObject();
//		slots[c].GetComponent<Image>().sprite = ItemImage[currentI];
//		slots[c].SetActive (true);
//	
//		Tool [currentI].SetActive (false);
//		slotList [c] = true;
//	}
//	
//	
//	public void DropTool(int k){
////		for (int x=0; x<TOTAL; x++) {
////			if (slots[k].GetComponent<Image>().sprite == ItemImage[x])
////			{
////				Tool[x].SetActive(true);
////				slots[k].GetComponent<Image>().sprite = null;
////				slots[k].SetActive(false);
////				slotList[k] = false;
////				
////				if (x == 9)
////				{
////					earCheckB = false;
////				}
////				if (x == 10)
////				{
////					eyeCheckB = false;
////				}
////			}
////		}
////		
////		slots[k].GetComponent<Image>().color = Color.white;
//	}
//	
//	public void UseTool(int n) {
//		for (int i = 0; i < TOTAL; i++)
//		{
////			if (slots[n].GetComponent<Image>().sprite == ItemImage[i])
////			{
////				if (i == 9)
////				{
////					earCheckB = true;
////					SetMsg("ear plug put on", Color.green);
////					slots[n].GetComponent<Image>().color = Color.green;
////				}
////				if (i == 10)
////				{
////					eyeCheckB = true;
////					SetMsg("eye goggle put on", Color.green);
////					slots[n].GetComponent<Image>().color = Color.green;
////				}
////				
////				
////			}
//		}
//		
//		
//	}
//	
//	public void SetActive (GameObject g)
//	{
//		if (g.activeSelf == true) {
//			g.SetActive (false);
//		} else {
//			g.SetActive (true);
//		}
//	}
//	
//	public void SetMsg(string m , Color c)
//	{
//		msgB = true;
//		msgText = m;
//		msgDisplay.GetComponent<Text>().color = c;
//	}
//	
//	void Update () {
//		if(Input.GetMouseButtonDown(0))
//		{
//			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//			RaycastHit hit;
//			
//			//Debug.Log("a= " + a);
//			if (Physics.Raycast (ray,out hit, 1000)) 
//			{
//				Debug.Log("tool: "+hit.collider.name);
//				
//				if(hit.transform.tag == "Tool" && !PickupPanel.active)
//				{
//					string[] toolName = hit.collider.name.Split('_');
//					int i = int.Parse(toolName[0]);
//					
//					
//					PickupPanel.SetActive(true);
//					ToolImage.GetComponent<Image> ().sprite = ItemImage[i];
//					currentI = i;
//				}
//				
//			}
//		}
//		
//		
//		if (msgB) 
//		{
//			msgDisplay.SetActive(true);
//			msgDisplay.GetComponent<Text>().text = msgText;
//			msgTimer += Time.deltaTime;
//			
//			if (msgTimer > 3) 
//			{
//				msgDisplay.SetActive(false);
//				msgB = false;
//				msgTimer = 0;
//			}
//		}
//		
//		
//	}
//}

