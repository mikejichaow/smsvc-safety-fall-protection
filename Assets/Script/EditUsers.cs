﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class EditUsers : MonoBehaviour {
	public GameObject UserName;
	public GameObject Password;
	public GameObject ConfirmPassword;
	public GameObject Email;
	public Dropdown UsersList_Dropdown;
	public Toggle IsInstructor;

	//Buttons used for constraints
	public Button Start_Button;
	public Button Instruction_Button;
	public Button About_Button;
	public Button Edit_Button;
	public Button UserReport_Button;
	public Button Delete_Button;
	public Button Confirm_Button;
	public GameObject YesOrNo_Button;
	public GameObject OK_Button;

	public GameObject EditMessage_Panel;
	public Text EditMessage_Text;

	public GameObject Report_Panel;
	public Button Previous_Button;
	public Button Next_Button;
	public Text[] Report_Text=new Text[18];
	public int index;
	public List<string> reports = new List<string> ();

	private bool isEdit;
	
	private bool PasswordCorrect;
	private bool ConfirmPasswordCorrect;
	private bool EmailCorrect;

	private string[] Charactors = {"a","b","c","d","e","f","g","h","i","j","k","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
		"A","B","C","D","E","F","G","H","I","J","K","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
		"1","2","3","4","5","6","7","8","9","0"};

	public List<string> UsersList;
	// Use this for initialization
	public void Inits () {
		UsersList.Clear ();
		if (Directory.Exists (@"./Users")){
			FileInfo[] files=new DirectoryInfo(@"./Users").GetFiles("*",SearchOption.TopDirectoryOnly);
			foreach (FileInfo f in files){
				UsersList.Add(f.Name.Split ('.')[0]);
			}
			UsersList_Dropdown.options.Clear();
			foreach (string option in UsersList)
			{
				UsersList_Dropdown.options.Add(new Dropdown.OptionData(option));
			}
		}
		if (UsersList_Dropdown.value>=UsersList.Count){
			UsersList_Dropdown.value-=1;
		}
		OnValueChanged ();
	}

	void Start(){
		Inits ();
	}

	public void ButtonConstraints(bool b){
		Start_Button.interactable = b;
		Instruction_Button.interactable = b;
		About_Button.interactable = b;
	}

	public void MessageConstraints(bool b){
		Edit_Button.interactable = b;
		Delete_Button.interactable = b;
	}

	public void OnValueChanged(){
		reports.Clear ();
		if (UsersList_Dropdown.value >= 0) { 
			UsersList_Dropdown.captionText.text=UsersList[UsersList_Dropdown.value];
			UserName.GetComponent<InputField> ().text = UsersList [UsersList_Dropdown.value];
			if (UsersList [UsersList_Dropdown.value] == "admin") {
				Delete_Button.interactable = false;
				Edit_Button.interactable = false;
				UserReport_Button.interactable=false;
			} else {
				Delete_Button.interactable = true;
				Edit_Button.interactable = true;
			}
			Password.GetComponent<InputField> ().interactable = false;
			ConfirmPassword.GetComponent<InputField> ().interactable = false;
			Email.GetComponent<InputField> ().interactable = false;
			IsInstructor.interactable = false;
			if (File.Exists (@"./Users/" + UsersList [UsersList_Dropdown.value] + ".txt")) {
				//Get password and user's position from user file
				string password = "";
				StreamReader reader = new StreamReader (@"./Users/" + UsersList [UsersList_Dropdown.value] + ".txt");
				string[] lines=reader.ReadToEnd ().Split('*');
				foreach (string s in lines) {
					if (s.Contains ("Time Spent")) {
						reports.Add (s);
					}
				}
				if (reports.Count<1){
					UserReport_Button.interactable=false;
				}
				else{
					UserReport_Button.interactable=true;
				}
				foreach (string line in (lines[0].Split('\n'))) {
					if (line.Contains ("Password:")) {
						//password=line;
						password = line.Substring (10);
						//Password Encrypt
						string PW = "";
						int i = 1;
						foreach (char c in password) {
							PW += ((char)(c - i)).ToString ();
							i++;
						}
						Password.GetComponent<InputField> ().text = PW;
						ConfirmPassword.GetComponent<InputField> ().text = PW;
					}
					if (line.Equals ("Position: Instructor")) {         //Get user's position
						IsInstructor.isOn = true;
					} else {
						IsInstructor.isOn = false;
					}
					if (line.Contains ("Email:")) {
						Email.GetComponent<InputField> ().text = line.Substring (7);
					}
				}
				reader.Close ();
			}
		}
	}

	public void DeleteUser(){
		File.Move (@"./Users/" + UsersList [UsersList_Dropdown.value] + ".txt",@"./Users/Removed Users/" + UsersList [UsersList_Dropdown.value] + ".txt");
		//File.Delete (@"./Users/" + UsersList [UsersList_Dropdown.value] + ".txt");
		Inits ();
		EditMessage_Text.text = "Delete complete.";
		YesOrNo_Button.SetActive (false);
		OK_Button.SetActive (true);
		EditMessage_Panel.SetActive (true);
		MessageConstraints (false);
	}

	public void EditUser(){
		EditMessage_Text.text = "";
		if (Password.GetComponent<InputField> ().text != "" && ConfirmPassword.GetComponent<InputField> ().text != "" && Email.GetComponent<InputField> ().text != "") {
			if (Password.GetComponent<InputField> ().text.Length > 5) {
				PasswordCorrect = true;
				if (ConfirmPassword.GetComponent<InputField> ().text.Equals (Password.GetComponent<InputField> ().text)) {
					ConfirmPasswordCorrect = true;
				} else {
					EditMessage_Text.text += "Passwords don't match.\n";
				}
			} else {
				PasswordCorrect = false;
				EditMessage_Text.text += "Password should have at least 6 charactors\n";
			}
			if (Email.GetComponent<InputField> ().text.Contains ("@") && Email.GetComponent<InputField> ().text.Contains (".")) {
				bool starts = false;
				foreach (string c in Charactors) {
					if (Email.GetComponent<InputField> ().text.StartsWith (c)) {
						starts = true;
						break;
					} else {
						starts = false;
					}
				}
				if (starts) {
					foreach (string c in Charactors) {
						if (Email.GetComponent<InputField> ().text.EndsWith (c)) {
							EmailCorrect = true;

							break;
						} else {
							EmailCorrect = false;
						}
					}
				} else {
					//LoginMessage.text+="Invaild Email Address.";
					EmailCorrect = false;
				}
			} else {
				//LoginMessage.text+="Invaild Email Address.";
				EmailCorrect = false;
			}
			if (EmailCorrect == false) {
				EditMessage_Text.text += "Invaild Email Address.";
			}
		} else {
			EditMessage_Text.text = "You should fill all the blanks.";
			EditMessage_Panel.SetActive (true);
			OK_Button.SetActive (true);
			YesOrNo_Button.SetActive(false);
			MessageConstraints(false);
		}

		if (PasswordCorrect == true && ConfirmPasswordCorrect == true && EmailCorrect == true) {
			
			//Password Encrypt
			string password = "";
			int i = 1;
			foreach (char c in Password.GetComponent<InputField>().text) {
				password += ((char)(c + i)).ToString ();
				i++;
			}

			//Write the infomation into file
			StreamReader reader = new StreamReader (@"./Users/" + UsersList[UsersList_Dropdown.value] + ".txt", true);
			string[] info=reader.ReadToEnd().Split ('\n');
			reader.Close();
			StreamWriter writer = new StreamWriter (@"./Users/" + UsersList[UsersList_Dropdown.value] + ".txt", false);
			info[1]="Email: " + Email.GetComponent<InputField> ().text;
			info[2]="Password: " + password;
			//writer.WriteLine ("Username: " + UserName.GetComponent<InputField> ().text);
			//writer.WriteLine ("Email: " + Email.GetComponent<InputField> ().text);
			//writer.WriteLine ("Password: " + password);
			if (IsInstructor.GetComponent<Toggle> ().isOn == false) {
				info[3]="Position: Employee";
				//writer.WriteLine ("Position: Employee");
			} else {
				info[3]="Position: Instructor";
				//writer.WriteLine ("Position: Instructor");
			}
			foreach (string s in info){
				writer.WriteLine(s);
			}
			writer.Close ();

			//Edit complete
			EditMessage_Text.text = "Edit Complete.";
			EditMessage_Panel.SetActive (true);
			OK_Button.SetActive (true);
			YesOrNo_Button.SetActive(false);
			MessageConstraints (false);
			Edit_Button.gameObject.SetActive (true);
			Confirm_Button.gameObject.SetActive (false);
			OnValueChanged();
		} else {
			EditMessage_Panel.SetActive (true);
			OK_Button.SetActive (true);
			YesOrNo_Button.SetActive(false);
			MessageConstraints (false);
		}
	}

	public void YesButton(){
		if (isEdit == true) {
			Password.GetComponent<InputField> ().interactable = true;
			ConfirmPassword.GetComponent<InputField> ().interactable = true;
			Email.GetComponent<InputField> ().interactable = true;
			IsInstructor.interactable = true;
			Edit_Button.gameObject.SetActive(false);
			Confirm_Button.gameObject.SetActive (true);
		} else {
			Edit_Button.gameObject.SetActive(true);
			Confirm_Button.gameObject.SetActive (false);
			DeleteUser ();
		}
	}

	public void PopUpMessage(string s){
		if (s.Equals("edit")){
			isEdit=true;
		}
		else{
			isEdit=false;
		}
		EditMessage_Text.text = "Do you want to " + s + " this user?";
		EditMessage_Panel.SetActive (true);
		OK_Button.SetActive (false);
		YesOrNo_Button.SetActive(true);
		MessageConstraints (false);
	}

	public void ReadReportFromFile(){
		string[] s = reports [index].Split ('\n');
		if (index + 1 < reports.Count) {
			Next_Button.interactable = true;
		} else {
			Next_Button.interactable = false;
		}
		if (index == 0) {
			Previous_Button.interactable = false;
		} else {
			Previous_Button.interactable = true;
		}
		Report_Text [0].text = s [1];
		Report_Text [1].text = s [2];
		for (int i=0,j=2; i<s.Length; i++) {
			if (s [i].Contains ("|")) {
				Report_Text [j].text = (s [i].Split ('|') [2]).Trim();
				j++;
			}
		}
		Report_Panel.SetActive (true);
	}

	public void setIndex(int i){
		index += i;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)) {
			if (UserName.GetComponent<InputField>().isFocused==true)
			{
				Password.GetComponent<InputField>().Select();
			}
			if (Password.GetComponent<InputField>().isFocused==true)
			{
				ConfirmPassword.GetComponent<InputField>().Select();
			}
			if (ConfirmPassword.GetComponent<InputField>().isFocused==true)
			{
				Email.GetComponent<InputField>().Select();
			}
		}
		if (UsersList_Dropdown.value < 0) {
			Delete_Button.interactable = false;
			Edit_Button.interactable = false;
			UserReport_Button.interactable = false;
		} else if (UsersList[UsersList_Dropdown.value]!="admin"){
			Delete_Button.interactable = true;
			Edit_Button.interactable = true;
			//UserReport_Button.interactable = true;
		}
	}
}
