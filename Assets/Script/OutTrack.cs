﻿using UnityEngine;
using System.Collections;

public class OutTrack : MonoBehaviour {
    public GameObject OutTrackPanel, warningSource;
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame

    void OnCollisionEnter(Collision player)
    {
        OutTrackPanel.SetActive(true);
        warningSource.GetComponent<AudioSource>().Play();
    }

    void OnCollisionExit(Collision player)
    {
        OutTrackPanel.SetActive(false);
        warningSource.GetComponent<AudioSource>().Stop();

    }
}
