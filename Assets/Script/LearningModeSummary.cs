﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LearningModeSummary : MonoBehaviour {
	
	public int imageNumber;
	public Sprite[] Screenshot;
	public GameObject PreviousButton;
	public GameObject NextButton;
	public GameObject SummaryImage;
	public GameObject Description;
	public GameObject NextRestart;
	
	// Use this for initialization
	void Start () 
	{
		imageNumber = 0;
	}
	
	// Update is called once per frame
	void Update () {
		switch (imageNumber) 
		{
		case 0:
			PreviousButton.SetActive(false);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[0];
			Description.GetComponent<Text>().text="Learned to move.";
			break;

		case 1:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[1];
			Description.GetComponent<Text>().text="Selected PPE.";
			break;

		case 2:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[2];
			Description.GetComponent<Text>().text="Chose additional equipment.";
			break;

		case 3:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[3];
			Description.GetComponent<Text>().text="Tied off to anchor point.";
			break;

		case 4:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[4];
			Description.GetComponent<Text>().text="Attach beamclamp.";
			break;

		case 5:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[5];
			Description.GetComponent<Text>().text="Released first lanyard.";
			break;

		case 6:
			PreviousButton.SetActive(true);
			NextButton.SetActive(true);
			SummaryImage.GetComponent<Image>().sprite=Screenshot[6];
			Description.GetComponent<Text>().text="Completed work.";
			break;

		case 7:
			SummaryImage.GetComponent<Image>().sprite=Screenshot[7];
			PreviousButton.SetActive(true);
			NextButton.SetActive(false);
			Description.GetComponent<Text>().text="Click 'Next' to go back to main menu.";
			NextRestart.SetActive(true);
			break;


		


		}
	}
	
	public void PreviousImage()
	{
		if (imageNumber >= 1) 
		{
			imageNumber=imageNumber-1;
		}
	}
	
	public void NextImage()
	{
		if (imageNumber <= 7) 
		{
			imageNumber=imageNumber+1;
		}
	}
}
