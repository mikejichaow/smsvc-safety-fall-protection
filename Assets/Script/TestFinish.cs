﻿using UnityEngine;
using System.Collections;

public class TestFinish : MonoBehaviour {
	public GameObject Player;
	public GameObject PlayerNext;
	public GameObject FadeImage;
	public GameObject Startwork;
    public GameObject scriptHolder;
	public GameObject landPanel;
    public GameObject panel_die;
    public GameObject panel_success;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider other)
	{
        if (scriptHolder.GetComponent<Runtime>().hookCounter == 0) {
            TriggerFinish();
        }       
	}

    void TriggerFinish() 
    {
        Player.transform.position = PlayerNext.transform.position;
        FadeImage.SetActive(true);
        Startwork.SetActive(false);
        scriptHolder.GetComponent<Runtime>().Headtext.gameObject.SetActive(false);
        landPanel.SetActive(true);
        if (!scriptHolder.GetComponent<Runtime>().hookUsageB || !scriptHolder.GetComponent<Runtime>().goodHarnessB || !scriptHolder.GetComponent<Runtime>().goodLanyardB||scriptHolder.GetComponent<Runtime>().deathcount>0)
        {
            panel_die.SetActive(true);
        }
        else
        {
            panel_success.SetActive(true);
        }
        WorkAreaTest.timerstart = false;

    }

}
