﻿using UnityEngine;
using System.Collections;

public class WrongWay : MonoBehaviour {

	public GameObject wrongwayPanel,warningSource,_wrongway;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter(Collider other){	
		    wrongwayPanel.SetActive (true);
		    warningSource.GetComponent<AudioSource> ().Play ();
		//_wrongway.SetActive (false);
	}

	void OnTriggerExit(Collider other){	
			wrongwayPanel.SetActive (false);
			warningSource.GetComponent<AudioSource> ().Stop ();
			_wrongway.SetActive (false);
	}
}
	