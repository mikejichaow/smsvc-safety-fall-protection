﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class Runtime : MonoBehaviour {

	public GameObject StartCanvas;
	public GameObject Canvas;

	public GameObject Control_Panel;
	public static bool ShowControlPanel=true;
	private static bool FirstTimeLearningMode=true;

	public GameObject door;
	public static float Timer;
	public Text TimerText;
	public bool Timerstop=true;
	private float minutes;
	private float seconds;
    bool randCTRL;
	private bool RaycastController;
	public GameObject Player;
	public GameObject PlayerNext;
	public GameObject PlayerGround;
	public GameObject Steam;
	//public GameObject Steam2;

	public GameObject EnterButton;
	public GameObject BackToGroundButton;
	public static List<string> Actionarray=new List<string>();

	public GameObject Scriptholder;
	public GameObject TrigggerworkerScript;
	
	//=====================================Timer====================================

	public GameObject player;
	public GameObject Lanyard1;
	public GameObject Lanyard2;
	public GameObject lanyardhook1;
	public GameObject lanyardhook2;
	public GameObject select;

	public float Dist1;
	public float Dist2;
	public float Dist3;
	public float windLevel;
	public float yesWind;
	public float noWind;


	public GameObject WholeLanyard1;
	public GameObject WholeLanyard1_1;

	public GameObject WholeLanyard2;
	public GameObject WholeLanyard2_1;
	public Transform lanyardoriginal1;
	public Transform lanyardoriginal2;
	public Transform Hookplace1;
	public Transform Hookplace2;

	//public GameObject WholeLanyard2;
	public GameObject Hook1;
	public GameObject Hook2;
	public GameObject Hook3;
	public GameObject MessagePanel;
	public GameObject MessageText;
	public GameObject MessagePanel1;     //Max length panel
	public GameObject MessageText1;
	public GameObject EscPanel;
	public GameObject TaskFailPanel;

	public GameObject currentItem;
    public GameObject currentHook;
    public GameObject currentAnchor;
    public float currentHookHeight;
    public bool movetoAnchorB;
	 
    public Hook hook1;
    public Hook hook2;

	public GameObject Beamclamp1;
	public GameObject Beamclamp2;
	public GameObject Lock;
	public GameObject point;
	public GameObject Beamstrap2;
	public GameObject warningSource,hurtSound;

	private bool BeamclampUsed;
	private bool BeamstrapUsed;
	public bool LanyardUsed;

	public bool lanyardmode1;
	public bool lanyardmode2;
	public bool lanyardrelease1;
	public bool lanyardrelease2;

	public bool Hookposition1;
	public bool Hookposition2;
	public bool Hookposition3;

	public static bool Comfirm;

	private Vector3 _HookpositionTarget1;
	private Vector3 _HookpositionTarget2;
	private Vector3 _HookpositionTarget3;
	private Vector3 _LanyardHook1Position;
	private Vector3 _LanyardHook2Position;
	//=================================================================================public UI===========================================================================================
	public Text Headtext;
	public  Text FailText;

    public int hookCounter;
	public int safecount;

	public GameObject Lanyard1Button;
	public GameObject Lanyard2Button;
	private Animator dooranimation;

    public GameObject doorEnterTest;

    public GameObject Panel_clampOff;
    public GameObject Panel_strapOff;
    public GameObject Panel_strap;
    public GameObject Panel_clamp;
    public GameObject Panel_Hook;

    public GameObject fadeImage;
    public GameObject Panel_fall;
    public GameObject panel_survive;
    public GameObject panel_die;
	public GameObject panel_rescue;
	public Text Panel_survive_Text;
	public Text panel_rescue_Text;

	public Sprite FallWithoutLanyard;
	public Sprite FallWithBadEquipment;
	public Sprite FallSlippery;

	public GameObject Button_Resume;
	public GameObject Button_ReturnToWork;
	public GameObject Button_CallForHelp;

    bool firstTime_Hook = true;
    bool firstTime_Clamp = true;
    bool firstTime_Strap = true;

	public bool[] ppes=new bool[5];
    public bool hookUsageB;
    public bool goodHarnessB;
	public bool goodHelmetB;
    public bool goodLanyardB;
    public bool clampRetrieveB;
    public bool strapRetrieveB;

	public bool ReportToSupervisor;
	public bool InspectToolsHarness;

    public GameObject panel_report;
	public GameObject panel_Login;
	public GameObject report_safer;
    public GameObject report_time;
    public GameObject report_PPE;
    public GameObject report_harness;
    public GameObject report_lanyard;
    public GameObject report_order;
    public GameObject report_fall;
    public GameObject report_clamp;
    public GameObject report_strap;
    public GameObject report_countdeath;
	public GameObject report_ReportToSupervisor;
	public GameObject report_inspect;
	public GameObject[] report_PPEs=new GameObject[5];
    public int deathcount;

	public GameObject ModeWarningText;
	public GameObject ModeOption_Panel;
	public GameObject Last_Panel;
	public GameObject Start_Panel;
	//======================================================================================Toggle==========================================================================================
	//public GameObject _NoWind_Toggle;
	//public GameObject _YesWind_Toggle;
	public GameObject TaskOption_Panel;
	public int TaskNo=0;
	public GameObject TaskMessage_Panel;
	public Text TaskMessage_Text;
	public GameObject FinishTask_Button;
	public GameObject TaskResults_Panel;
	public Text TaskResults_Text;

	private bool ClickedLanyard;
	public Texture Texture_Lanyard;

	public bool NoWind_Toggle;
	public bool YesWind_Toggle;


	public GameObject _LearningMode;
	public GameObject _TestingMode;
	public GameObject _ShortMode;

	public  static bool LearningMode=false;
	public static bool TestingMode=true;
	public bool ShortMode=false;
	//================================================================================================Learning mode=============================================================================================
	public int step1;
	public int step2;
	public int step3;

	public Sprite[] StepImage;

	public GameObject Arrow1;
	public GameObject Arrow2;
	public GameObject Arrow3;
	public GameObject Arrow4;
	public GameObject Arrow5;
	public GameObject Arrow6;
	public GameObject Arrow7;
	public GameObject Arrow8;
	public GameObject Arrow9;
	public GameObject Arrow10;

	public GameObject step1_text;
	public GameObject step2_text;
	public GameObject step3_text;

	public GameObject step1_image;
	public GameObject step2_image;
	public GameObject step3_image;

	public GameObject Button_Continue1;
	public GameObject Button_Continue2;

	public GameObject step3_point1;
	public GameObject step3_point2;
	public GameObject step3_point3;
	public GameObject step3_point4;
	public GameObject step3_point5;
	public GameObject step3_point6;

	public GameObject Report_Panel1;
	public GameObject Report_Panel2;

	public int score = 100;
	public bool isAutoFailed=false;
	public Text score_Text;
	//==========================================================================================================================================================================================================

	//ArrayList Actionarray=new ArrayList();
	//public Animator dooranimation;
	// Use this for initialization
	void Start () {
		//Lanyard1.transform.LookAt(_LanyardHook1Position);
		//Lanyard2.transform.LookAt(_LanyardHook2Position);
		Timer = 900.0f;
		yesWind = 5;
		windLevel=yesWind;
		noWind = 10;
        deathcount = 0;
		YesWind_Toggle = true;
		NoWind_Toggle = false;
		//TestingMode = true;
		Timerstop=true;
		BeamclampUsed = false;
		BeamstrapUsed = false;
		LanyardUsed = false;
		hookUsageB = true;
		RaycastController = false;
		if (TestingMode) {
			panel_Login.GetComponent<Login> ().Restart ();
		} 
		ClickedLanyard = false;
		//windLevel = yesWind;
		ppes = new bool[5];
		for (int i=0; i<5; i++) {
			ppes [i] = false;
		}
		if (LearningMode) {
			ShowControl (Control_Panel);
		}
	}

	public void ShowControl(GameObject controlPanel){
		if (ShowControlPanel == true) {
			controlPanel.SetActive (true);
			ShowControlPanel=false;
			RaycastControl (false);
			setRigidBodyFPSController (false);
		} else {
			controlPanel.SetActive (false);
			RaycastControl (true);
			setRigidBodyFPSController (true);
		}
	}

	public void ShortModeShowControlPanel(GameObject controlPanel){
		if (TaskNo == 0) {
			ShowControl (Control_Panel);
		} else {
			ShowControl (controlPanel);
		}
	}

	public void setRigidBodyFPSController(bool b){
		player.GetComponent<RigidbodyFirstPersonController>().enabled=b;
	}

	public void setIsAutoFailed(bool b){
		isAutoFailed = b;
	}

	public void RaycastControl(bool b){
		RaycastController = b;
	}
	
	public bool getRaycastController(){
		return RaycastController;
	}


	public void Check(){
		if (!LearningMode && !TestingMode && !ShortMode) {
			ModeWarningText.SetActive (true);
			ModeOption_Panel.SetActive (true);
			Last_Panel.SetActive (false);
		} else {
			if (ShortMode) {
				TaskOption_Panel.SetActive (true);
			} else {
				Last_Panel.SetActive (true);
			}
			ModeOption_Panel.SetActive (false);
			ModeWarningText.SetActive (false);
		}
	}

	public void CheckToStart()
	{
		if (TestingMode) {
			Debug.Log ("Start the Simulator");
			StartCanvas.SetActive (false);
			Canvas.SetActive (true);
			Start_Panel.SetActive (true);
			Timerstop = false;
			ShedTrigger.isToggle = true;
			ShowControl(Control_Panel);
			FirstTimeLearningMode=false;
		}

		if (LearningMode) {
			Debug.Log ("Start the Simulator");
			Application.LoadLevel ("LearningMode");
			if (FirstTimeLearningMode) {
				ShowControlPanel = true;
				FirstTimeLearningMode=false;
			}
		}

		if (ShortMode) {
			StartCanvas.SetActive (false);
			Canvas.SetActive (true);
			Start_Panel.SetActive(false);
			Timerstop = true;
			FinishTask_Button.SetActive(true);
			ShedTrigger.isToggle=false;
			FirstTimeLearningMode=false;
			New_InventorySystem inventory=gameObject.GetComponent<New_InventorySystem> ();
			switch (TaskNo){
			case 0:
				TaskMessage_Text.text="Task 1: Choose Correct Personal Protective Equipment(PPE).\nLook at the PPE on the table and wall. Choose the undamaged equipment and put it on. You can use \"Inspect\" button to see if the PPE is damaged. Click \"Finish\" when you are done.";
				TaskMessage_Panel.SetActive (true);
				break;
			case 1:
				Player.transform.position = new Vector3(-143.9f,-24.88f,370f);
				Player.transform.Rotate(0.0f,-30f,0.0f);
				inventory._Harness2.SetActive(true);
				inventory._Helmet2.SetActive(true);
				inventory.safetyGoggle.SetActive(true);
				inventory.earPlug.SetActive(true);
				inventory.BodyWithGloves.GetComponent<Renderer>().materials[1].mainTexture = inventory.WithGloves;
				TaskMessage_Text.text="Task 2: Be Aware of Mobile Equipment.\n Click the door to open it, and be careful of forklifts when crossing the road. Click \"Finish\" when you are done.";
				TaskMessage_Panel.SetActive (true);
				break;
			case 2:
				LearningmodeContinue1();
				TaskMessage_Text.text="Task 3: Choose Correct Equipments.\nLook at the equipments on the table. Choose the undamaged one and put it into inventory. You can use \"Inspect\" to see if the equipment is damaged. Besides you can choose to bring a buddy with you. Click \"Finish\" when you are done.";
				TaskMessage_Panel.SetActive (true);
				break;
			case 3:
				//New_InventorySystem inventory=gameObject.GetComponent<New_InventorySystem> ();
				LearningmodeContinue2();
				Beamclamp2.SetActive(true);
				inventory._Harness2.SetActive(true);
				inventory._Helmet2.SetActive(true);
				inventory.safetyGoggle.SetActive(true);
				inventory.earPlug.SetActive(true);
				inventory.BodyWithGloves.GetComponent<Renderer>().materials[1].mainTexture = inventory.WithGloves;
				inventory.Lanyard2.SetActive (true);
				inventory.Instructor2.SetActive(true);
				EnterButton.SetActive(true);
				TaskMessage_Text.text="Task 4: Choose Correct Anchor Points.\nChoose the correct anchor points for the hook to finish work safely. You can use the beam clamp as an assistant anchor. Click \"Finish\" when you are done.";
				TaskMessage_Panel.SetActive (true);
				break;
			}
		}
	}

	public void LearningmodeContinue1()
	{
		Player.transform.position = PlayerGround.transform.position;
		if (LearningMode) {
			Arrow5.SetActive (true);
		}
		New_InventorySystem inventory=gameObject.GetComponent<New_InventorySystem> ();
		//inventory._Harness.SetActive (true);
		inventory._Harness2.SetActive(true);
		//inventory._Helmet.SetActive(true);
		inventory._Helmet2.SetActive(true);
		inventory.safetyGoggle.SetActive(true);
		inventory.earPlug.SetActive(true);
		inventory.BodyWithGloves.GetComponent<Renderer>().materials[1].mainTexture = inventory.WithGloves;
	}

	public void LearningmodeContinue2()
	{
		Player.transform.position = PlayerNext.transform.position;
		Player.transform.rotation = PlayerNext.transform.rotation;
		New_InventorySystem inventory=gameObject.GetComponent<New_InventorySystem> ();
		inventory.Lanyard1.SetActive (true);
		//inventory.Lanyard2.SetActive (true);

	}

	public void ReturnToWork(){
		if (hookCounter < 1 || !goodHarnessB || !goodLanyardB) {
			Player.transform.position = PlayerNext.transform.position;
			Player.transform.rotation = PlayerNext.transform.rotation;
			doorEnterTest.GetComponent <TestEnter> ().gateEntered = false;
			hookCounter=0;
			WholeLanyard1_1.transform.parent=Hookplace1;
			WholeLanyard2_1.transform.parent=Hookplace2;
			WholeLanyard1_1.transform.position = lanyardoriginal1.position;
			WholeLanyard1_1.transform.rotation = lanyardoriginal1.rotation;
			WholeLanyard2_1.transform.position = lanyardoriginal2.position;
			WholeLanyard2_1.transform.rotation = lanyardoriginal2.rotation;
		}
	}

	public void FinishTask(){
		switch (TaskNo) {
		case 0:
			int i;
			for (i=0; i<5; i++) {
				if (ppes[i]==false){
					break;
				}
			}
			if (i<5){
				int j=2;
				TaskResults_Text.text="You failed this task.\nReason:\n\t1.You haven't wear all PPE.";
				if (!goodHarnessB){
					TaskResults_Text.text+="\t"+j+".You wore damaged harness.\n";
					j++;
				}
				if (!goodHelmetB){
					TaskResults_Text.text+="\t"+j+".You wore damaged harness.\n";
					j++;
				}
			}else{
				int j=1;
				if (goodHarnessB&&goodHelmetB){
					TaskResults_Text.text="Congratulations! You passed this task.";
				}
				else{
					TaskResults_Text.text="You failed this task.\nReason:\n";
					if (!goodHarnessB){
						TaskResults_Text.text+="\t"+j+".You wore damaged harness.\n";
						j++;
					}
					if (!goodHelmetB){
						TaskResults_Text.text+="\t"+j+".You wore damaged harness.\n";
						j++;
					}
				}
			}
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		}
	}


	// Update is called once per frame
	void Update () 
	{
		if (TestingMode==true&&BackToGroundButton.activeSelf == true) {
			if (hookCounter < 1) {
				BackToGroundButton.GetComponent<Button> ().interactable = true;
			} else {
				BackToGroundButton.GetComponent<Button> ().interactable = false;
			}
		}
		if (LearningMode&&Application.loadedLevelName=="LearningMode") {
			windLevel=noWind;
			switch (step1) {
			case 0:
				Arrow1.SetActive (true);
	
				step1_text.GetComponent<Text> ().text = "Use Keyboard 'W' to move forward, use mouse to look around.";
				step1_image.GetComponent<Image> ().sprite = StepImage [0];
				break;

			case 1:
				Arrow1.SetActive (false);
				Arrow2.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Ok. \nNext use Keyboard 'A' to move left";
				step1_image.GetComponent<Image> ().sprite = StepImage [0];
				break;
			case 2:
				Arrow2.SetActive (false);
				Arrow3.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Well done! \nUse Keyboard 'D' to move right";
				step1_image.GetComponent<Image> ().sprite = StepImage [1];
				break;

			case 3:
				Arrow3.SetActive (false);
				Arrow4.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Seems you already know how to move.  \nClick on the gloves and put on it";
				step1_image.GetComponent<Image> ().sprite = StepImage [0];
				break;
			//LearningMode=true;

			case 4:
				Arrow4.SetActive (false);
				Arrow5.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Now you're wearing the gloves.  \nClick on the harness and put on it";
				step1_image.GetComponent<Image> ().sprite = StepImage [0];
				break;

			case 5:
				Arrow5.SetActive (false);
				Arrow6.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Now you weared the harness.  \nClick on the helmet and put on it";
				step1_image.GetComponent<Image> ().sprite = StepImage [1];
				break;

			case 6:
				Arrow6.SetActive (false);
				Arrow7.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Now you're wearing the helmet.  \nClick on the glasses on the wall and put on it";
				step1_image.GetComponent<Image> ().sprite = StepImage [1];
				break;

			case 7:
				Arrow7.SetActive (false);
				Arrow8.SetActive (true);
				step1_text.GetComponent<Text> ().text = "Now you're wearing the glasses.  \nClick on the ear plug-in and put on it";
				step1_image.GetComponent<Image> ().sprite = StepImage [1];
				break;

			case 8:
				Arrow8.SetActive (false);
				step1_text.GetComponent<Text> ().text = "Good job! \nYou are ready to walk out. \nClick 'continue' to next step!";
				Button_Continue1.SetActive (true);
				break;

			}
	
			switch (step2) {
			case 0:
			    Arrow9.SetActive (true);
				step2_text.GetComponent<Text> ().text = "Now you are choosing fall protection equipment.  \nPlease click on the lanyard and add to your inventory.";
				step2_image.GetComponent<Image> ().sprite = StepImage [0];
				break;

			case 1:
				Arrow9.SetActive (false);
				Arrow10.SetActive (true);
				step2_text.GetComponent<Text> ().text = "Next,pick up the beamclamp and add to your inventory";
				step2_image.GetComponent<Image> ().sprite = StepImage [0];
				break;
			case 2:
				Arrow10.SetActive (false);
				step2_text.GetComponent<Text> ().text = "Almost done, let's click 'Continue' to start work ";
				step2_image.GetComponent<Image> ().sprite = StepImage [1];
				Button_Continue2.SetActive (true);
				break;
			}

			switch (step3) {
			case 0:
				step3_text.GetComponent<Text> ().text = "Now you are on the work area. \n Move close to the gate. \nClick on the lanyard first";
				step3_point1.SetActive(true);
				step3_image.GetComponent<Image> ().sprite = StepImage [0];
				break;
				
			case 1:
				step3_text.GetComponent<Text> ().text = "Next, click on the red hook.";
				//Panel_Hook.SetActive(true);
				step3_point1.SetActive(false);
				step3_point2.SetActive(true);
				step3_image.GetComponent<Image> ().sprite = StepImage [0];
				break;
			case 2:
				step3_text.GetComponent<Text> ().text = "Now you are under protection. Click 'Enter'.";
				EnterButton.SetActive(true);
				step3_image.GetComponent<Image> ().sprite = StepImage [1];
			
				break;
			case 3:
				Beamclamp2.GetComponent<Drag>().enableDrag = true;
				step3_text.GetComponent<Text> ().text = "Use mouse wheel (or '<' '>' key)to move the beamclamp to target point. \n Then click the red spot to tighten it. ";
				step3_image.GetComponent<Image> ().sprite = StepImage [1];
				step3_point6.SetActive(true);
				
				break;
			case 4:

				step3_text.GetComponent<Text> ().text = "Click on another lanyard and attach to the beamclamp.";
		
				step3_image.GetComponent<Image> ().sprite = StepImage [0];
				
				break;

			case 5:
				step3_text.GetComponent<Text>().text="Click the first lanyard and release it.";
				step3_image.GetComponent<Image> ().sprite = StepImage [0];
				step3_point1.SetActive(true);
				break;

			case 6:
				step3_text.GetComponent<Text>().text="Put it back.";
				step3_image.GetComponent<Image> ().sprite = StepImage [1];
				step3_point5.SetActive(true);
				step3_point1.SetActive(false);
				
				break;

            case 7:
                step3_text.GetComponent<Text>().text = "Now walk to the work area.";
                step3_image.GetComponent<Image>().sprite = StepImage[1];
                step3_point5.SetActive(false);

                break;

            case 8:
                    Debug.Log("step3=8");
                step3_text.GetComponent<Text>().text = "Wait until the work finished.";
                step3_image.GetComponent<Image>().sprite = StepImage[1];              

                break;

            }

		}

		if (Scriptholder.GetComponent<New_InventorySystem> ().ReportHarness [1] || Scriptholder.GetComponent<New_InventorySystem> ().ReportHarness [2] || Scriptholder.GetComponent<New_InventorySystem> ().ReportHarness [6]||Scriptholder.GetComponent<New_InventorySystem>().ReportTools[4]||Scriptholder.GetComponent<New_InventorySystem>().ReportTools[5]||Scriptholder.GetComponent<New_InventorySystem>().ReportTools[6]) 
		{
			ReportToSupervisor=true;
		}
		
		if (Scriptholder.GetComponent<New_InventorySystem> ().InspectHarness [1] || Scriptholder.GetComponent<New_InventorySystem> ().InspectHarness [2] || Scriptholder.GetComponent<New_InventorySystem> ().InspectHarness [6] || Scriptholder.GetComponent<New_InventorySystem> ().ReportTools [4] || Scriptholder.GetComponent<New_InventorySystem> ().ReportTools [5] || Scriptholder.GetComponent<New_InventorySystem> ().ReportTools [6]) 
		{
			InspectToolsHarness=true;
		}

        if (!Timerstop)
        {

            Timer -= Time.deltaTime;
        }
            minutes =(int) Timer / 60;
            seconds =(int) Timer % 60;

		if (seconds < 0) {
			TimerText.text = "Time Out";
			TimerText.fontSize -= 4;
		} else {
			TimerText.text = "Time left \n" + string.Format ("{0:0}m:{1:0}s", minutes, seconds);
		}

		if (minutes <10) {
			TimerText.color = new Color(1.0f,0.45f,0.0f);
			if (Mathf.Abs(Timer)%60*10%10>7)
			{
				TimerText.enabled=false;
			}
			else{
				TimerText.enabled=true;
			}
		}

		if (seconds==35&&TestingMode==true)
		{
			var systems=Steam.GetComponentsInChildren<ParticleSystem>();
			foreach (var system in systems)
			{
				system.enableEmission = false;
			}
		}
		if (seconds==10&&TestingMode==true)
		{
			//Steam.SetActive (true);
			var systems=Steam.GetComponentsInChildren<ParticleSystem>();
			foreach (var system in systems)
			{
				system.enableEmission = true;
			}
		}

//		Actionarray.Add ("Timestart");
		//===================================================================================

//		if (TestEnter.TaskFail) 
//		{
//			Debug.Log("Task failed!");
//			TaskFailPanel.SetActive(true);
//			TestEnter.TaskFail=false;
//		}

		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			EscPanel.SetActive(true);
		}

		Dist1 = Vector3.Distance (Lanyard1.transform.position,Hook1.transform.position);
		Dist2 = Vector3.Distance (Lanyard2.transform.position,Hook2.transform.position);
		Dist3 =	Vector3.Distance (Lanyard1.transform.position,Hook3.transform.position);






		//Debug.Log ("Distance1 is :"+ Dist1);
		_HookpositionTarget1 =Hook1.transform.position;
		_HookpositionTarget2 =Hook2.transform.position;
		_HookpositionTarget3 =Hook3.transform.position;
		_LanyardHook1Position = lanyardhook1.transform.position;
		_LanyardHook2Position = lanyardhook2.transform.position;

		//Lanyard1.transform.LookAt(_LanyardHook1Position);
		//lanyardhook1.transform.LookAt (_HookpositionTarget1);
		if (RaycastController==true){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Input.GetMouseButtonDown (0)) 
		{
			if (Physics.Raycast (ray, out hit, 1000)) 
			{
				/* ===============   make all hook tag to "Hook"  ==========
                 
                if (hit.transform.tag == "Hookposition1") 
				{
					if(!Hookposition3&&!Hookposition2)
					{
						Hookposition1=true;
					}
					lanyardrelease1=false;
				}
			
				if (hit.transform.tag == "Hookposition2")
				{
					if(!Hookposition1&&!Hookposition3)
					{
						Hookposition2=true;
					}
					lanyardrelease2=false;
				}
			
				if (hit.transform.tag == "Hookposition3") 
				{
					if(!Hookposition1&&!Hookposition2)
					{
						Hookposition3=true;
					}
				}

                
                */

                /* 
				if (hit.transform.tag == "BeamClamp") 
				{
					//select.SetActive(true);
					New_InventorySystem.checkBeamclamp=false;
					Beamclamp1.SetActive(false);
					Debug.Log("Hit beamclamp and ready to use");
					Beamclamp2.SetActive(true);
					//else{
					//	New_InventorySystem.checkBeamclamp=true;
					//	Beamclamp1.SetActive(false);
					//}
				}
				
                */


				if (hit.transform.tag == "Point") 
				{
					Lock.transform.Translate(-0.5f,0,0);
					point.SetActive(false);
                    Beamclamp2.GetComponent<Drag>().enableDrag = false;
					step3=4;
					Scriptholder.GetComponent<Runtime>().step3_point6.SetActive(false);

                }

                if (hit.transform.tag == "BeamClamp")
                {
                    Panel_clampOff.SetActive(true);
                }

                if (hit.transform.name == "BeamStrap_Onframe")
                {
                    Panel_strapOff.SetActive(true);
                }


                if (hit.collider.tag == "Hook")
                {
					if(step3==0||step3==5)
					{
						step3=step3+1;
					}
                    if (firstTime_Hook) {
                        Panel_Hook.SetActive(true);
                        firstTime_Hook = false;
						step3_point1.SetActive(false);
					
                    }
                    if(currentHook == null)
                    {
                        if (Vector3.Distance(player.transform.position, hit.collider.gameObject.transform.position) < 18f)
                        {
                            GameObject aHook = hit.collider.gameObject;
                            currentHook = aHook;
                            currentHookHeight = aHook.transform.position.y;
                            Headtext.GetComponent<Text>().text = "You are operating: " + currentHook.name;
							ClickedLanyard=true;
                        }
                        else if (Vector3.Distance(player.transform.position, hit.collider.gameObject.transform.position) >= 18f)
                        {
								RaycastController=false;
								MessagePanel.SetActive(true);
								MessageText.GetComponent<Text>().text = "You can not reach it. Walk Closer";
                        }
                    }
                    else if (currentHook == hit.collider.gameObject)
                    {
                        currentHook = null;
                        Headtext.GetComponent<Text>().text = "You are operating: ";
						ClickedLanyard=false;
                    }

                }

                if (hit.collider.tag == "Anchor")
                {
					step3_point2.SetActive(false);
                    if (currentHook != null)
                    {
                        if (Vector3.Distance(currentHook.transform.position, hit.collider.gameObject.transform.position) < 13f)
                        {
							if(step3==1||step3==4||step3==6)
							{
								step3=step3+1;
							}
                            currentAnchor = hit.collider.gameObject;
                            movetoAnchorB = true;

                            Headtext.GetComponent<Text>().text = "You are tied off : " + currentAnchor.name;
							ClickedLanyard=false;
                        }
                        else if (Vector3.Distance(currentHook.transform.position, hit.collider.gameObject.transform.position) >= 13f)
                        {
                            MessagePanel.SetActive(true);
                            MessageText.GetComponent<Text>().text = "You can not reach it. Walk Closer";
                        }
                    }
                }

			}
			}
		}
        //====================

        if (movetoAnchorB)
        {
            currentHook.transform.LookAt(currentAnchor.transform);
            currentHook.transform.position = Vector3.MoveTowards(currentHook.transform.position, currentAnchor.transform.position, 15f * Time.deltaTime);
            if (Vector3.Distance(currentHook.transform.position, currentAnchor.transform.position) < 2.2f)
            {
                movetoAnchorB = false;
                currentHook.transform.parent = currentAnchor.transform;
                if (currentAnchor.name == "HookPlace1") {
					if(hookCounter<1)
					{
						hookUsageB=false;
					}
                    currentHook.transform.position = lanyardoriginal1.position;
                    currentHook.transform.rotation = lanyardoriginal1.rotation;
                    Headtext.GetComponent<Text>().text = currentHook.name + " released.";
                }
                else if (currentAnchor.name == "HookPlace2")
                {
					if(hookCounter<1)
					{
						hookUsageB=false;
					}
					currentHook.transform.position = lanyardoriginal2.position;
                    currentHook.transform.rotation = lanyardoriginal2.rotation;
                    Headtext.GetComponent<Text>().text = currentHook.name + " released.";
                }
                if (currentAnchor.name != "RailAnchor")
                {
                    float anchorHeightDiff = currentHookHeight - currentAnchor.transform.position.y;
                    print(anchorHeightDiff);

                    if (anchorHeightDiff > 4)
                    {
                        hookCounter--;
						if (hookCounter<1)
						{
							deathcount++;
						}
                    }
                    if (anchorHeightDiff < -4)
                    {
                        hookCounter++;
                    }
                }
                currentHook = null;
            }
        }

        //test 6 ft length
        if (currentAnchor != null)
        {
			if (Vector3.Distance(WholeLanyard1_1.transform.position, player.transform.position) > 23f)
			{
				//Forward and Back constraint
				if (player.transform.right.z<0.1f&&player.transform.position.x-WholeLanyard1_1.transform.position.x<0||player.transform.right.z>-0.1f&&player.transform.position.x-WholeLanyard1_1.transform.position.x>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.ForwardSpeed=14.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.BackwardSpeed=0.0f;
				}
				if (player.transform.right.z>-0.1f&&player.transform.position.x-WholeLanyard1_1.transform.position.x<0||player.transform.right.z<0.1f&&player.transform.position.x-WholeLanyard1_1.transform.position.x>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.BackwardSpeed=6.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.ForwardSpeed=0.0f;
				}
				//Right and Left constraint
				if (player.transform.right.z>-0.1f&&player.transform.position.z-WholeLanyard1_1.transform.position.z<0||player.transform.right.z<0.1f&&player.transform.position.z-WholeLanyard1_1.transform.position.z>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.LeftSpeed=0.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.RightSpeed=4.0f;
				}
				if (player.transform.right.z<0.1f&&player.transform.position.z-WholeLanyard1_1.transform.position.z<0||player.transform.right.z>-0.1f&&player.transform.position.z-WholeLanyard1_1.transform.position.z>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.RightSpeed=0.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.LeftSpeed=4.0f;
				}
				MessagePanel1.SetActive(true);
				MessageText1.GetComponent<Text>().text = "You can not move further. Lanyard at max length";
			}
			
			if (Vector3.Distance(WholeLanyard2_1.transform.position, player.transform.position) > 23f){
				//Forward and Back constraint
				if (player.transform.right.z<0.1f&&player.transform.position.x-WholeLanyard2_1.transform.position.x<0||player.transform.right.z>-0.1f&&player.transform.position.x-WholeLanyard2_1.transform.position.x>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.ForwardSpeed=14.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.BackwardSpeed=0.0f;
				}
				if (player.transform.right.z>-0.1f&&player.transform.position.x-WholeLanyard2_1.transform.position.x<0||player.transform.right.z<0.1f&&player.transform.position.x-WholeLanyard2_1.transform.position.x>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.BackwardSpeed=6.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.ForwardSpeed=0.0f;
				}
				
				//Right and Left constraint
				if (player.transform.right.z>-0.1f&&player.transform.position.z-WholeLanyard2_1.transform.position.z<0||player.transform.right.z<0.1f&&player.transform.position.z-WholeLanyard2_1.transform.position.z>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.LeftSpeed=0.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.RightSpeed=4.0f;
				}
				if (player.transform.right.z<0.1f&&player.transform.position.z-WholeLanyard2_1.transform.position.z<0||player.transform.right.z>-0.1f&&player.transform.position.z-WholeLanyard2_1.transform.position.z>0)
				{
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.RightSpeed=0.0f;
					player.GetComponent<RigidbodyFirstPersonController>().movementSettings.LeftSpeed=4.0f;
				}
				MessagePanel1.SetActive(true);
				MessageText1.GetComponent<Text>().text = "You can not move further. Lanyard at max length";
			}
			if (Vector3.Distance(WholeLanyard1_1.transform.position, player.transform.position)<=23f&&Vector3.Distance(WholeLanyard2_1.transform.position, player.transform.position)<=23f)
			{
				player.GetComponent<RigidbodyFirstPersonController>().movementSettings.ForwardSpeed=14.0f;
				player.GetComponent<RigidbodyFirstPersonController>().movementSettings.BackwardSpeed=6.0f;
				player.GetComponent<RigidbodyFirstPersonController>().movementSettings.RightSpeed=4.0f;
				player.GetComponent<RigidbodyFirstPersonController>().movementSettings.LeftSpeed=4.0f;
				MessagePanel1.SetActive(false);
			}

        }


		//======================================================================================================================================================================
        //if (TestEnter.TestStart) 
        //{
        //    if(!lanyardmode1&&!lanyardmode2&&TestEnter.TestStart)
        //    {
        //        TestEnter.TaskFail=true;
        //        FailText.text="You are not under protection!\n\nYou will die!";
        //    }
        //}

        //==========

        //========check swing gate enter
       

		if (doorEnterTest.GetComponent<TestEnter>().gateEntered) {
            if (hookCounter < 1) {
                hookUsageB = false;
				if(LearningMode==true&&!warningSource.GetComponent<AudioSource> ().isPlaying){
					warningSource.GetComponent<AudioSource> ().Play();
				}
			}
			TriggerFall();
		}

      

        //===========

		if (New_InventorySystem.checkBeamclamp) 
		{
			Beamclamp2.SetActive(true);
			BeamclampUsed=true;
            Beamclamp2.GetComponent<Drag>().enableDrag = true;
            if (firstTime_Clamp) {
                Panel_clamp.SetActive(true);
                firstTime_Clamp = false;
            }
            New_InventorySystem.checkBeamclamp = false;
		}

        //if (New_InventorySystem.checkLifeline&&Comfirm) 
        //{

        //    Lanyard1Button.SetActive(true);
        //    Lanyard2Button.SetActive(true);
        //}

        //if (New_InventorySystem.checkUsedLanyard&&Comfirm) 
        //{
        //    Debug.Log("Used the old lanyard it dangerous");
        //    Lanyard1Button.SetActive(true);
        //    Lanyard2Button.SetActive(true);
        //}

//		if (New_InventorySystem.checkUsedLanyard && Comfirm) 
//		{
//			Debug.Log("Test the used lanyard");
//			Lanyard1Button.SetActive(true);
//			Lanyard2Button.SetActive(true);
//		}


		if (New_InventorySystem.checkBeanstrap) 
		{
			Beamstrap2.SetActive(true);
			BeamstrapUsed=true;
            if (firstTime_Strap) {
                Panel_strap.SetActive(true);
                firstTime_Strap = false;
            }
            New_InventorySystem.checkBeanstrap = false;
		}


        //if (New_InventorySystem.checkS_Lifeline||New_InventorySystem.checkS_Used_Lifeline) 
        //{
        //    Lanyard1Button.SetActive(true);
        //}

       

        /*


		if (Hookposition1&&lanyardmode1) 
		{
			if (Dist1 > 13f) 
			{
				MessagePanel.SetActive (true);
				MessageText.GetComponent<Text>().text="You can not move further. Lanyard at max length";
                player.transform.Translate(0, 0, -1);
			}

			lanyardhook1.transform.position=Vector3.MoveTowards(lanyardhook1.transform.position,_HookpositionTarget1,2.2f*Time.deltaTime);
			lanyardhook1.transform.parent=Hook1.transform;
			WholeLanyard1.transform.LookAt(_LanyardHook1Position);

		}
		if (Hookposition1&&lanyardmode2) 
		{
			if (Dist1 > 13f) 
			{
				MessagePanel.SetActive (true);
                MessageText.GetComponent<Text>().text = "You can not move further. Lanyard at max length";
                player.transform.Translate(0, 0, -1);
			}


			lanyardhook2.transform.position=Vector3.MoveTowards(lanyardhook2.transform.position,_HookpositionTarget1,2.2f*Time.deltaTime);
			lanyardhook2.transform.parent=Hook1.transform;
			WholeLanyard2.transform.LookAt(_LanyardHook2Position);
			lanyardhook2.transform.LookAt(_HookpositionTarget1);
		

		}

		if (Hookposition2&&lanyardmode1) 
		{
			if (Dist2 > 12f) 
			{
				MessagePanel.SetActive (true);
				MessageText.GetComponent<Text>().text="You can not move before far before finding another anchor point";
			}

			lanyardhook1.transform.position=Vector3.MoveTowards(lanyardhook2.transform.position,_HookpositionTarget2,2.2f*Time.deltaTime);
			lanyardhook1.transform.parent=Hook2.transform;
			lanyardhook1.transform.LookAt(_HookpositionTarget2);
		}


		if (Hookposition2&&lanyardmode2) 
		{
			if (Dist2 > 14f) 
			{
				MessagePanel.SetActive (true);
				MessageText.GetComponent<Text>().text="You can not move before far before finding another anchor point";
			}

			lanyardhook2.transform.position=Vector3.MoveTowards(lanyardhook2.transform.position,_HookpositionTarget2,2.2f*Time.deltaTime);
			lanyardhook2.transform.parent=Hook2.transform;
	
			lanyardhook2.transform.LookAt(_HookpositionTarget2);
		}

		if (Hookposition3 && lanyardmode1) 
		{
			if (Dist3 > 17f) 
			{
				MessagePanel.SetActive (true);
				MessageText.GetComponent<Text>().text="You can not move before far before finding another anchor point";
			}

			lanyardhook1.transform.position=Vector3.MoveTowards(lanyardhook1.transform.position,_HookpositionTarget3,2.2f*Time.deltaTime);
			lanyardhook1.transform.parent=Hook3.transform;
			WholeLanyard1.transform.LookAt(_LanyardHook1Position);
		}


		if (Hookposition3 && lanyardmode2) 
		{
			if (Dist3 > 16f) 
			{
				MessagePanel.SetActive (true);
				MessageText.GetComponent<Text>().text="You can not move before far before finding another anchor point";
			}

			lanyardhook2.transform.position=Vector3.MoveTowards(lanyardhook1.transform.position,_HookpositionTarget3,2.2f*Time.deltaTime);
			lanyardhook2.transform.parent=Hook3.transform;
			WholeLanyard2.transform.LookAt(_LanyardHook1Position);
		}
        */

		if (lanyardrelease1) 
		{
			Hookposition1 = false;
			lanyardhook1.transform.position=Vector3.MoveTowards(lanyardhook1.transform.position,lanyardoriginal1.transform.position ,2.2f*Time.deltaTime);
			lanyardhook1.transform.parent=WholeLanyard1_1.transform;


		}
		if (lanyardrelease2) 
		{
			Hookposition1 = false;
			lanyardhook2.transform.position=Vector3.MoveTowards(lanyardhook2.transform.position,lanyardoriginal2.transform.position ,1.5f*Time.deltaTime);
		lanyardhook2.transform.parent=WholeLanyard2_1.transform;
		}
	}

	void OnGUI(){
		if (ClickedLanyard && Panel_Hook.activeSelf == false && MessagePanel.activeSelf == false && Panel_fall.activeSelf == false) {
			GUI.DrawTexture (new Rect (Event.current.mousePosition.x - 15, Event.current.mousePosition.y - 15, 64, 64), Texture_Lanyard);
			Cursor.visible = false;
		} else {
			Cursor.visible = true;
		}
	}

	public void SetLearningMode()
	{
		if (!LearningMode) {
			_TestingMode.GetComponent<Toggle> ().isOn = false;
			_LearningMode.GetComponent<Toggle> ().isOn = true;
			_ShortMode.GetComponent<Toggle> ().isOn = false;
			LearningMode = true;
			TestingMode = false;
			ShortMode = false;
		} else {
			_LearningMode.GetComponent<Toggle> ().isOn = false;
			LearningMode = false;
			TestingMode = false;
			ShortMode = false;
		}
	}

	public void SetTestingMode()
	{
		if (!TestingMode) {
			_TestingMode.GetComponent<Toggle> ().isOn = true;
			_LearningMode.GetComponent<Toggle> ().isOn = false;
			_ShortMode.GetComponent<Toggle> ().isOn = false;
			LearningMode = false;
			TestingMode = true;
			ShortMode = false;
		} else {
			_TestingMode.GetComponent<Toggle> ().isOn = false;
			LearningMode = false;
			TestingMode = false;
			ShortMode = false;
		}
	}

	public void SetShortMode()
	{
		if (!ShortMode) {
			_TestingMode.GetComponent<Toggle> ().isOn = false;
			_LearningMode.GetComponent<Toggle> ().isOn = false;
			_ShortMode.GetComponent<Toggle> ().isOn = true;
			LearningMode = false;
			TestingMode = false;
			ShortMode = true;
		} else {
			_ShortMode.GetComponent<Toggle> ().isOn = false;
			LearningMode = false;
			TestingMode = false;
			ShortMode = false;
		}
	}

	public void setTask(int taskno){
		TaskNo = taskno;
	}

	/*public void setYesWind(){

		Debug.Log ("Wind is weak mode");
		//_WeakWind_Toggle.GetComponents<UnityEngine.UI.Toggle>().I
	
		if (!YesWind_Toggle) {
			_YesWind_Toggle.GetComponent<Toggle>().isOn=true;
			_NoWind_Toggle.GetComponent<Toggle>().isOn=false;
			YesWind_Toggle = true;
			NoWind_Toggle = false;
			windLevel = yesWind;
		} 
		else
		{
			_YesWind_Toggle.GetComponent<Toggle>().isOn=false;
			YesWind_Toggle= false;
			NoWind_Toggle = false;

		}


	}

	public void setNoWind(){
		//windLevel = noWind;

		if (!NoWind_Toggle) {
			_NoWind_Toggle.GetComponent<Toggle>().isOn=true;
			_YesWind_Toggle.GetComponent<Toggle>().isOn=false;
			YesWind_Toggle = false;
			NoWind_Toggle = false;
			NoWind_Toggle = true;
			windLevel = noWind;
		} 
		else
		{
			_NoWind_Toggle.GetComponent<Toggle>().isOn=false;
			YesWind_Toggle = false;
			NoWind_Toggle = false;

		}

		Debug.Log ("No wind mode");
	}*/

    public void TimerCTRL(bool b) {
        Timerstop = b;
    }

    public void TriggerFall() 
    {
		if (safecount < 2) 
		{
			if (seconds%10==0&&hookCounter < 1 || !goodHarnessB || !goodLanyardB) {
				isAutoFailed=true;
				safecount++;
				if (Login.isInstructor==true){
					Button_Resume.SetActive(true);
				}
				else{
					Button_Resume.SetActive(false);
				}
				if (hookCounter<1){
					Panel_fall.GetComponent<Image>().sprite=FallWithoutLanyard;
				}
				else{
					Panel_fall.GetComponent<Image>().sprite=FallWithBadEquipment;
				}
				hurtSound.GetComponent<AudioSource> ().Play();
				Panel_fall.SetActive(true);
				Button_CallForHelp.SetActive(false);
				panel_die.SetActive (true);
				deathcount = deathcount + 1;
				RigidbodyFirstPersonController.IsMoving=false;
			}
			else 
			{
				if (seconds%5==0&&randCTRL==true&&Panel_fall.activeSelf==false&&TestingMode==true) {
					if (RigidbodyFirstPersonController.IsMoving==true){
					int r = Random.Range(0, 10);
					print("r=" + r);
				if (r>windLevel){
					Panel_fall.SetActive(true);
					Panel_fall.GetComponent<Image>().sprite=FallSlippery;
					fadeImage.SetActive(true);
					hurtSound.GetComponent<AudioSource> ().Play();
					Panel_survive_Text.text="The surface was very slippery, and you fell. You are wearing proper fall protection equipment";
					if (gameObject.GetComponent<New_InventorySystem>().GrabBuddy==true){
						Panel_survive_Text.text+=", and your buddy rescued you.";
						Button_Resume.SetActive(true);
					}
					else{
						if (gameObject.GetComponent<New_InventorySystem>().BringRadio==true){
							Panel_survive_Text.text+=", and you brought the radio with you. However the radio fell, and you are stuck. You should bring a buddy next time.";
						}
						else {
							Panel_survive_Text.text+=". However you are stuck, you should grab a buddy with you.";
						}
						if (Login.isInstructor==true){
							Button_Resume.SetActive(true);
						}
						else{
							Button_Resume.SetActive(false);
						}
					}
					Button_CallForHelp.SetActive(true);
					deathcount++;
				/*	}
					else
					{
						Panel_survive_Text.text="The surface is very slippery, but luckily you were tied off.";
						Button_CallForHelp.SetActive(false);
						Button_Resume.SetActive(true);
					}*/
					panel_survive.SetActive (true);
					panel_die.SetActive(false);
					//Panel_fall.SetActive(true);
					safecount++;
						}
					randCTRL = false;
					}
				}
				else if ((int)seconds % 5 == 2)
				{
					randCTRL = true;
				}
		}
	}
//        report_fall.GetComponent<Text>().text = "Yes";
    }

	public void CallForRescue(){
		panel_rescue.SetActive (true);
		panel_survive.SetActive (false);
		Button_ReturnToWork.SetActive(true);
		Button_Resume.SetActive (false);
		panel_rescue_Text.text = "You used the radio to call your buddy for help, and you were resucued.";
	}

	public void ConfirmToGo()
	{
		Comfirm = true;
	}


	public void opendoor()
	{
        /*door.GetComponentInChildren<BoxCollider>().enabled = false;
		dooranimation = door.GetComponent<Animator> ();
		dooranimation.enabled = true;*/
		if (ShortMode) {
			Beamclamp2.GetComponent<Drag> ().enableDrag = true;
		} else {
			Beamclamp2.GetComponent<Drag> ().enableDrag = false;
		}
		door.GetComponentInChildren<AudioSource> ().Play ();
		step3 =3;
        MessagePanel.SetActive (true);
        MessageText.GetComponent<Text>().text = "You shouldn’t enter this area until you are tied off to an appropriate anchor point.";
		door.transform.localRotation = Quaternion.Euler (270, 90, 0);
    

	}
	public void Closedoor()
	{
		/*door.GetComponentInChildren<BoxCollider>().enabled = false;
		dooranimation = door.GetComponent<Animator> ();
		dooranimation.enabled = true;*/
		door.GetComponentInChildren<AudioSource> ().Play ();
		step3 =3;
		door.transform.localRotation = Quaternion.Euler (270, 0, 0);
		
		
	}
	
	//	public void backtoGround()
//	{
//		Timer -= 180f;
//		Comfirm = false;
//		TrigggerworkerScript.GetComponent<StartWorkTest> ().Player.transform.position = TrigggerworkerScript.GetComponent<StartWorkTest> ().PlayerNext.transform.position;
//		TrigggerworkerScript.GetComponent<StartWorkTest> ().Player.transform.rotation = TrigggerworkerScript.GetComponent<StartWorkTest> ().PlayerNext.transform.rotation;
//		TrigggerworkerScript.GetComponent<StartWorkTest> ().Enterbutton.SetActive (false);
//		TrigggerworkerScript.GetComponent<StartWorkTest> ().Backbutton.SetActive (false);
//		TrigggerworkerScript.GetComponent<StartWorkTest> ().FadeImage.SetActive (true);
//
//	}

	public void ActiveSelf(GameObject g)
	{
		if (g.activeSelf) 
		{
			g.SetActive(false);
		}
		else
		{
			g.SetActive(true);
		}
	}

	public void checktools()
	{
		//if ( Usetool2 && Usetool3) {
		
		MessagePanel.SetActive (true);
		MessageText.GetComponent<Text>().text="Are you ready to start the task? \n\nClick the 'Inventory' button to check all the tools you selected. ";
//		if (!New_InventorySystem.checkBeamclamp && !New_InventorySystem.checkBeanstrap && !New_InventorySystem.checkLifeline) 
//		{
//			MessagePanel.SetActive (true);
//			MessagePanel.GetComponent<Text>().text="You are endanger, you will die!";
//		}
		
	}

	public void Lanyardmode1()
	{
		//WholeLanyard1.transform.LookAt (-_HookpositionTarget1);
		lanyardmode1 = true;
		lanyardmode2 = false;
		Headtext.text = "You are operating lanyard1";
		Hookposition1 = false;
		Hookposition2 = false;
		Hookposition3 = false;
		lanyardrelease1 = false;


	}

	public void Lanyard1Release ()
	{
		lanyardrelease1 = true;
		lanyardmode1 = false;
	}

	public void Lanyard2Release ()
	{
		lanyardrelease2 = true;
		lanyardmode2 = false;
	}


	public void Lanyardmode2()
	{
		lanyardmode2 = true;
		lanyardmode1 = false;
		Headtext.text = "You are operating lanyard2";
		Hookposition1 = false;
		Hookposition2 = false;
		Hookposition3 = false;
		lanyardrelease2 =false;
		
		
	}

	public void SliderValueChange(Slider s){
		if (s.value == 0) {
			Report_Panel1.SetActive(true);
			Report_Panel2.SetActive (false);
		} else if (s.value == 1) {
			Report_Panel1.SetActive (false);
			Report_Panel2.SetActive(true);
		}
	}

    public void ReportCard() 
    {
        float timeSpent = 900 - Timer;
        float min = timeSpent / 60f;
        float sec = timeSpent % 60f;
        print("timer: " + Timer + min + sec);
		report_PPE.GetComponent<Text>().text="Yes";
		for (int i=0; i<5; i++) {
			report_PPEs [i].GetComponent<Text> ().text = ppes [i] == true ? "Yes" : "No";
			if (ppes[i]==false){
				report_PPE.GetComponent<Text>().text="No";
				score-=10;
			}
		}
        report_time.GetComponent<Text>().text = string.Format("{0:0} m:{1:0} s", min, sec);
        report_harness.GetComponent<Text>().text = goodHarnessB == true ? "Yes" : "No";
		if (ppes [0] == true && goodHelmetB == false) {
			score -= 10;
		}
		//if (ppes [1] == true && goodHarnessB == false) {
		//	score -= 100;
		//}
		if (LanyardUsed == false) {
			report_lanyard.GetComponent<Text> ().text = "N/A";
			score -= 10;
		}
		else {
			report_lanyard.GetComponent<Text> ().text = goodLanyardB == true ? "Yes" : "No";
			if (goodLanyardB==false){
				score-=10;
			}
		}
        report_order.GetComponent<Text>().text =  hookUsageB == true ? "Yes" : "No";
		if (BeamclampUsed == false) {
			report_clamp.GetComponent<Text> ().text = "N/A";
		} else {
			if (point.activeSelf == true) {
				score -= 10;
			}
			report_clamp.GetComponent<Text> ().text = Beamclamp2.active == false ? "Yes" : "No";
			if (Beamclamp2.active==true){
				score-=10;
			}
		}
		if (BeamstrapUsed == false) {
			report_strap.GetComponent<Text> ().text = "N/A";
		} else {
			report_strap.GetComponent<Text> ().text = Beamstrap2.active == false ? "Yes" : "No";
			if (Beamstrap2.active==true){
				score-=10;
			}
		}

		if (isAutoFailed == false) {
			score_Text.text = score.ToString ();
			score_Text.resizeTextMaxSize=85;
		} else {
			score_Text.text = "Failed";
			score_Text.resizeTextMaxSize=58;
		}

        Timerstop = true;
        report_countdeath.GetComponent<Text>().text = deathcount.ToString();
		
		report_ReportToSupervisor.GetComponent<Text> ().text = ReportToSupervisor == true ? "Yes" : "No";
		report_inspect.GetComponent<Text>().text= InspectToolsHarness==true? "Yes":"No";	
        panel_report.SetActive(true);
		setRigidBodyFPSController (false);
    }

	public void WriteReportToFile(){
		//Write Report to File
		string[] results = new string[15];
		string[] ppes_name = {"Helmet","Harness","Gloves","Ear Plug","Safety Glasses"};
		results [0] = string.Format ("  | {0,-30} | {1,-30} |","Time Spent:",report_time.GetComponent<Text> ().text);
		results [1] = string.Format ("  | {0,-30} | {1,-30} |","Wear PPE:",report_PPE.GetComponent<Text> ().text);
		for (int i=2; i<6; i++) {
			results [i] = string.Format ("  |   {0,-28} | {1,-30} |", "Wear "+ppes_name[i-2]+":", report_PPEs[i-2].GetComponent<Text> ().text);
		}
		results [6] = string.Format ("  | {0,-30} | {1,-30} |","Good Condition Harness:",report_harness.GetComponent<Text> ().text);
		results [7] = string.Format ("  | {0,-30} | {1,-30} |","Good Condition Lanyard:" , report_lanyard.GetComponent<Text> ().text);
		results [8] = string.Format ("  | {0,-30} | {1,-30} |","Proper Lanyard Sequence:" , report_order.GetComponent<Text> ().text);
		results [9] = string.Format ("  | {0,-30} | {1,-30} |","Retrieve Beam Clamp:" , report_clamp.GetComponent<Text> ().text);
		results [10] = string.Format ("  | {0,-30} | {1,-30} |","Retrieve Beam Strap:" , report_strap.GetComponent<Text> ().text);
		results [11] = string.Format ("  | {0,-30} | {1,-30} |","Inspect All Tools:" , report_inspect.GetComponent<Text> ().text);
		results [12] = string.Format ("  | {0,-30} | {1,-30} |","Incident Count:" , report_countdeath.GetComponent<Text> ().text);
		results [13] = string.Format ("  | {0,-30} | {1,-30} |","Report to Supervisor" , report_ReportToSupervisor.GetComponent<Text> ().text);
		results [14] =string.Format ("  | {0,-63} |","How could we make the job safer?");
		//results [11] = string.Format ("| {0,-63} |",report_safer.GetComponent<InputField> ().text);
		//results [1] = "Wear PPE:" + report_PPE.GetComponent<Text> ().text;
		/*results [2] = "Good Condition Harness:" + report_harness.GetComponent<Text> ().text;
		results [3] = "Good Condition Lanyard:" + report_lanyard.GetComponent<Text> ().text;
		results [4] = "Proper Lanyard Sequence:" + report_order.GetComponent<Text> ().text;
		results [5] = "Retrieve Beam Clamp:" + report_clamp.GetComponent<Text> ().text;
		results [6] = "Retrieve Beam Strap:" + report_strap.GetComponent<Text> ().text;
		results [7] = "Inspect All Tools:" + report_inspect.GetComponent<Text> ().text;
		results [8] = "Incident Count:" + report_countdeath.GetComponent<Text> ().text;
		results [9] = "Report to Supervisor" + report_ReportToSupervisor.GetComponent<Text> ().text;
		results [10] = "How could we make the job safer?";
		results [11] = report_safer.GetComponent<InputField> ().text;*/
		panel_Login.GetComponent<Login> ().WriteResultsToFile (results,report_safer.GetComponent<InputField> ().text,score_Text.text);
	}
	
	//=======================================================================================================================
	public void Quit()
	{
		Application.Quit ();
	}


	public void SMSVC_Link()
	{
		Application.OpenURL("http://steelconsortium.org/");
	}

	public void CIVS_Link()
	{
		Application.OpenURL("http://centers.pnw.edu/civs/");
	}

	public void Survey_Link()
	{
		Application.OpenURL("https://purdue.qualtrics.com/jfe/form/SV_5nZI7N46OzLaqSV");
	}

	public void Restart()
	{
		score = 100;
		TestingMode = true;
		LearningMode = false;
		Application.LoadLevel (0);
		TaskNo = 0;
		//Lanyard1Button.SetActive (false);
		//Lanyard2Button.SetActive (false);
		TestEnter.TaskFail = false;
		hookUsageB = true;
		BeamclampUsed = false;
		BeamstrapUsed = false;
		LanyardUsed = false;
		RaycastController = false;
		//TestEnter.TestStart = false;
		New_InventorySystem.checkBeamclamp = false;
		New_InventorySystem.checkBeanstrap = false;
		New_InventorySystem.checkHarness = false;
		New_InventorySystem.checkHarnessB1 = false;
		New_InventorySystem.checkHarnessB2 = false;
		New_InventorySystem.checkLifeline = false;
		New_InventorySystem.checkS_Lifeline = false;
		New_InventorySystem.checkS_Used_Lifeline = false;
		New_InventorySystem.checkUsedLanyard = false;
		panel_Login.GetComponent<Login> ().Restart ();
		//TaskFailPanel.SetActive (false);
	}
}
