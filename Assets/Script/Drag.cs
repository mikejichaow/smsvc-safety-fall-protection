﻿using UnityEngine;
using System.Collections;

public class Drag : MonoBehaviour {
	
	public GameObject target;
    public bool enableDrag;

	void Update()
	{
        if (enableDrag)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                target.transform.position = new Vector3(transform.position.x + Input.GetAxis("Mouse ScrollWheel"), transform.position.y, transform.position.z);

            }

			if(Input.GetKeyDown(KeyCode.Period))
			{
				target.transform.position = new Vector3(transform.position.x -0.5f, transform.position.y, transform.position.z);
			}

			if(Input.GetKeyDown(KeyCode.Comma))
			{
				target.transform.position = new Vector3(transform.position.x +0.5f, transform.position.y, transform.position.z);
			}

        }
	}
}
