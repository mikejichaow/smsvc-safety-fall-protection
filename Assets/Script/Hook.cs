﻿using UnityEngine;
using System.Collections;

public class Hook : MonoBehaviour {

    public GameObject modelMesh;
    public GameObject followedObject;
    public bool changeAnchor;

    public bool hooked;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (changeAnchor) {
            modelMesh.transform.position = Vector3.MoveTowards(modelMesh.transform.position, followedObject.transform.position, 3f * Time.deltaTime);
            if (Vector3.Distance(modelMesh.transform.position, followedObject.transform.position) < 0.1f)
            {
                changeAnchor = false;
            }
        }
	
	}
}
