﻿using UnityEngine;
using System.Collections;

public class Flashing : MonoBehaviour {

	float Timer=900.0f;
	public GameObject Warning;
	
	// Update is called once per frame
	void Update () {
		Timer -= Time.deltaTime;
		if (Timer % 60 * 10 % 10 > 7) {
			Warning.SetActive (false);
		} else {
			Warning.SetActive (true);
		}
	
	}
}
