﻿using UnityEngine;
using System.Collections;

public class StartWorkTest : MonoBehaviour {

	public GameObject Player;
	public GameObject PlayerNext;
	public GameObject PlayerGround;
	public GameObject Enterbutton;
	public GameObject Backbutton;
	public GameObject FadeImage;

    public GameObject panel_handRail;
	public GameObject panel_inspect;


	public GameObject _TestEnter;

	// Use this for initialization
	void OnTriggerEnter (Collider other)
	{
		//panel_inspect.SetActive (true);
        panel_handRail.SetActive(true);
	}

	void OnTriggerExit(Collider other)
	{
		//panel_inspect.SetActive (false);
		panel_handRail.SetActive(false);
	}

    public void StartWork()
    {
        Runtime.Timer -= 60f;
        Runtime.Comfirm = true;
        Player.transform.position = PlayerNext.transform.position;
        Player.transform.rotation = PlayerNext.transform.rotation;
        
        Enterbutton.SetActive(true);
		Backbutton.SetActive (true);
        FadeImage.SetActive(true);
        
    }
	
	public void BackToGround()
	{
		Runtime.Timer -= 60f;
		Runtime.Comfirm = false;
		Player.transform.position = PlayerGround.transform.position;
		Player.transform.rotation = PlayerGround.transform.rotation;
		
		Enterbutton.SetActive(false);
		Backbutton.SetActive (false);
		FadeImage.SetActive(true);
		_TestEnter.GetComponent<TestEnter> ().gateEntered = false;
		GameObject.Find ("Scriptholder").GetComponent<Runtime> ().safecount = 0;

		
	}



}
