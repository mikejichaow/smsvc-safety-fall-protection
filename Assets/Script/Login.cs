﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class Login : MonoBehaviour {
	// Using for constraints
	public GameObject StartButton;
	public GameObject InstructionsButton;
	public GameObject AboutButton;

	//Text
	public GameObject UserName;
	public GameObject Password;
	public GameObject ConfirmPassword;
	public GameObject Email;
	public GameObject Button_Login;
	public GameObject Button_Visitor;
	public GameObject Button_Logout;
	public GameObject Button_AddUsers;
	public GameObject Button_UsersList;
	public GameObject Button_Back;
	public GameObject Toggle_IsInstructor;

	public Button Button_Register;
	public Button Button_Login1;
	
	public GameObject Panel_LoginMessage;
	public Text LoginMessage;

	public GameObject WelcomeUser;
	private static string User;

	private bool UsernameCorrect;
	private bool PasswordCorrect;
	private bool ConfirmPasswordCorrect;
	private bool EmailCorrect;

	private bool isRegister;           //Check if it is log in or register
	public static bool isInstructor;

	private string[] Lines;
	private string[] Charactors = {"a","b","c","d","e","f","g","h","i","j","k","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
		                           "A","B","C","D","E","F","G","H","I","J","K","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
		                           "1","2","3","4","5","6","7","8","9","0"};

	public void setIsRegister(bool b){
		isRegister = b;
	}

	public string getUser(){
		return User;
	}

	public void MessageConstraints(bool b){
		Button_Login1.interactable = b;
		Button_Register.interactable = b;
	}

	public void Back(){
		if (User == "" || User == null) {
			WelcomeUser.SetActive (false);
			Button_Logout.SetActive (false);
			Button_Back.SetActive (true);
		} else {
			StartButton.SetActive (true);
			InstructionsButton.SetActive (true);
			AboutButton.SetActive (true);
			WelcomeUser.GetComponent<Text> ().text = "Welcome, " + User;
			WelcomeUser.SetActive (true);
			Button_Logout.SetActive (true);
			if (isInstructor) {
				Button_AddUsers.SetActive (true);
				Button_UsersList.SetActive(true);
			}
		}
	}
	
	public void Restart(){
		if (User == ""||User==null) {
			WelcomeUser.SetActive (false);
			Button_Logout.SetActive (false);
			Button_Login.SetActive (true);
			Button_Visitor.SetActive (true);
			Button_AddUsers.SetActive(false);
			Button_UsersList.SetActive(false);
		} else {
			StartButton.SetActive (true);
			InstructionsButton.SetActive (true);
			AboutButton.SetActive (true);
			if (isInstructor==true){
				Button_AddUsers.SetActive(true);
				Button_UsersList.SetActive (true);
			}
			else
			{
				Button_AddUsers.SetActive(false);
				Button_UsersList.SetActive (false);
			}
			WelcomeUser.GetComponent<Text>().text="Welcome, "+User;
			WelcomeUser.SetActive (true);
			Button_Logout.SetActive (true);
			Button_Login.SetActive (false);
			Button_Visitor.SetActive (false);
		}
	}

	void Start () {
		isRegister = false;
		UsernameCorrect = false;
		PasswordCorrect = false;
		ConfirmPasswordCorrect = false;
		EmailCorrect = false;
		if (!Directory.Exists (@"./Users")) {
			Directory.CreateDirectory (@"./Users");
		}
		if (!Directory.Exists (@"./Users/Removed Users")) {
			Directory.CreateDirectory (@"./Users/Removed Users");
		}
		if (!File.Exists (@"./Users/admin.txt")) {
			StreamWriter writer = new StreamWriter (@"./Users/admin.txt", true);
			writer.WriteLine ("Username: admin");
			writer.WriteLine ("Email: ");
			writer.WriteLine ("Password: bfpms");
			writer.WriteLine ("Position: Instructor");
			writer.WriteLine ("***************************************************************************");
			writer.Close ();
		}

	}
	
	public void setLoginMessage(){
		LoginMessage.text = "";
	}

	public void OKButton(){
		//StartButton.GetComponent<Button> ().interactable = true;
		//InstructionsButton.GetComponent<Button> ().interactable = true;
		AboutButton.GetComponent<Button> ().interactable = true;
		Button_Visitor.GetComponent<Button> ().interactable = true;
		Button_Login.GetComponent<Button> ().interactable = true;
	}

	public void LoginConstraint(bool b){
		Button_Visitor.GetComponent<Button> ().interactable = b;
		Button_Login.GetComponent<Button> ().interactable = b;
		//StartButton.GetComponent<Button> ().interactable = gameObject.activeSelf;
		//InstructionsButton.GetComponent<Button> ().interactable = gameObject.activeSelf;
		AboutButton.GetComponent<Button> ().interactable = b;
		UserName.GetComponent<InputField> ().text = "";
		Password.GetComponent<InputField> ().text = "";
		ConfirmPassword.GetComponent<InputField> ().text = "";
		Email.GetComponent<InputField> ().text = "";
	}

	public void LoginButton(){                     //Login Function
		//print ("Log In\n");
		if (UserName.GetComponent<InputField> ().text != "" && Password.GetComponent<InputField> ().text != "") {
			if (File.Exists (@"./Users/" + UserName.GetComponent<InputField> ().text + ".txt")) {
				//Get password and user's position from user file
				string line,password="";
				StreamReader reader=new StreamReader(@"./Users/" + UserName.GetComponent<InputField> ().text + ".txt");
				while ((line=reader.ReadLine())!=null){
					if (line.Contains ("Password:")){
						password=line;
					}
					if (line.Equals ("Position: Instructor")){         //Get user's position
						isInstructor=true;
						RigidbodyFirstPersonController.RunEnabled=true;
					}
					else{
						isInstructor=false;
						RigidbodyFirstPersonController.RunEnabled=false;
					}
					if (password!=""&&isInstructor==true){
						break;
					}
				}
				reader.Close();
				password=password.Substring (10);
				//Password Encrypt
				string PW="";
				int i=1;
				foreach (char c in Password.GetComponent<InputField>().text){
					PW+=((char)(c+i)).ToString();
					i++;
				}

				if (PW.Equals(password)){
					LoginMessage.text="Log In Successful.";
					User=UserName.GetComponent<InputField>().text;
					WelcomeUser.GetComponent<Text>().text="Welcome, "+User;
					WelcomeUser.SetActive(true);
					if (isInstructor){
						Button_AddUsers.SetActive(true);
						Button_UsersList.SetActive (true);
					}
					else
					{
						Button_AddUsers.SetActive(false);
						Button_UsersList.SetActive(false);
					}

					StartButton.SetActive (true);
					InstructionsButton.SetActive (true);
					AboutButton.SetActive (true);

					UserName.GetComponent<InputField>().text="";
					Password.GetComponent<InputField>().text="";
					gameObject.SetActive (false);
					Button_Login.SetActive(false);
					Button_Visitor.SetActive(false);
					Button_Logout.SetActive (true);
				}
				else
				{
					LoginMessage.text="Password Incorrect";
				}
				Panel_LoginMessage.SetActive(true);
				MessageConstraints(false);

			} else {
				LoginMessage.text = "User doesn't exist.";
				Panel_LoginMessage.SetActive (true);
				MessageConstraints(false);
			}
		} else {
			LoginMessage.text = "Please type in your username and password.";
			Panel_LoginMessage.SetActive (true);
			MessageConstraints(false);
		}
	}

	public void LogOutButton(){
		User = "";
		LoginMessage.text = "Log Out Successful";
		Panel_LoginMessage.SetActive (true);
		MessageConstraints(false);
		WelcomeUser.SetActive (false);
		Button_Login.SetActive (true);
		Button_Visitor.SetActive (true);
		Button_Logout.SetActive (false);
		Button_UsersList.SetActive (false);
		Button_AddUsers.SetActive (false);
		StartButton.SetActive (false);
		InstructionsButton.SetActive (false);
		AboutButton.SetActive (true);
		Runtime.ShowControlPanel = true;
	}

	public void RegisterButton(){                  //Register Function
		if (isRegister) {
			if (UserName.GetComponent<InputField> ().text != "" && Password.GetComponent<InputField> ().text != "" && ConfirmPassword.GetComponent<InputField> ().text != "" && Email.GetComponent<InputField> ().text != "") {
				if (File.Exists (@"./Users/" + UserName.GetComponent<InputField> ().text + ".txt")) {
					LoginMessage.text = "Username Taken. Please choose another user name.\n";
					UsernameCorrect = false;
					Panel_LoginMessage.SetActive (true);
					MessageConstraints(false);
					return;
				} else {
					UsernameCorrect = true;
				}
				if (Password.GetComponent<InputField> ().text.Length > 5) {
					PasswordCorrect = true;
					if (ConfirmPassword.GetComponent<InputField> ().text.Equals (Password.GetComponent<InputField> ().text)) {
						ConfirmPasswordCorrect = true;
					} else {
						LoginMessage.text += "Passwords don't match.\n";
					}
				} else {
					PasswordCorrect = false;
					LoginMessage.text += "Password should have at least 6 charactors\n";
				}
				if (Email.GetComponent<InputField> ().text.Contains ("@") && Email.GetComponent<InputField> ().text.Contains (".")) {
					bool starts = false;
					foreach (string c in Charactors) {
						if (Email.GetComponent<InputField> ().text.StartsWith (c)) {
							starts = true;
							break;
						} else {
							starts = false;
						}
					}
					if (starts) {
						foreach (string c in Charactors) {
							if (Email.GetComponent<InputField> ().text.EndsWith (c)) {
								EmailCorrect = true;
								break;
							} else {
								EmailCorrect = false;
							}
						}
					} else {
						//LoginMessage.text+="Invaild Email Address.";
						EmailCorrect = false;
					}
				} else {
					//LoginMessage.text+="Invaild Email Address.";
					EmailCorrect = false;
				}
				if (EmailCorrect == false) {
					LoginMessage.text += "Invaild Email Address.";
				}
			} else {
				LoginMessage.text = "You should fill all the blanks.";
				Panel_LoginMessage.SetActive (true);
				MessageConstraints(false);
			}
			if (UsernameCorrect == true && PasswordCorrect == true && ConfirmPasswordCorrect == true && EmailCorrect == true) {

				//Password Encrypt
				string password = "";
				int i = 1;
				foreach (char c in Password.GetComponent<InputField>().text) {
					password += ((char)(c + i)).ToString ();
					i++;
				}

				//Register Functions
				LoginMessage.text = "Registration Complete.";
				setIsRegister(false);
				Panel_LoginMessage.SetActive (true);
				MessageConstraints(false);
				gameObject.SetActive (false);

				//Write User info into file
				StreamWriter writer = new StreamWriter (@"./Users/" + UserName.GetComponent<InputField> ().text + ".txt", true);
				writer.WriteLine ("Username: " + UserName.GetComponent<InputField> ().text);
				writer.WriteLine ("Email: " + Email.GetComponent<InputField> ().text);
				writer.WriteLine ("Password: " + password);
				if (Toggle_IsInstructor.GetComponent<Toggle> ().isOn == false) {
					writer.WriteLine ("Position: Employee");
				} else {
					writer.WriteLine ("Position: Instructor");
				}
				writer.WriteLine ("***************************************************************************");
				writer.Close ();

				UserName.GetComponent<InputField> ().text = "";
				Password.GetComponent<InputField> ().text = "";
				ConfirmPassword.GetComponent<InputField> ().text = "";
				Email.GetComponent<InputField> ().text = "";
				Toggle_IsInstructor.GetComponent<Toggle> ().isOn = false;
			} else {
				Panel_LoginMessage.SetActive (true);
				MessageConstraints(false);
			}

			//print ("Registration\n");
		} /*else {
			if (isInstructor == true) {
				Toggle_IsInstructor.SetActive (true);
			}
		}*/

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)) {
			if (UserName.GetComponent<InputField>().isFocused==true)
			{
				Password.GetComponent<InputField>().Select();
			}
			if (isRegister){
				if (Password.GetComponent<InputField>().isFocused==true)
				{
				ConfirmPassword.GetComponent<InputField>().Select();
				}
				if (ConfirmPassword.GetComponent<InputField>().isFocused==true)
				{
					Email.GetComponent<InputField>().Select();
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.Return)) {
			if (isRegister == false) {
				if (UserName.GetComponent<InputField>().text!=""&&Password.GetComponent<InputField>().text!=""){
					LoginButton ();
				}
			}
			else
			{
				if (UserName.GetComponent<InputField>().text!=""&&Password.GetComponent<InputField>().text!=""&&ConfirmPassword.GetComponent<InputField>().text!=""&&Email.GetComponent<InputField>().text!=""){
					RegisterButton();
				}
			}
		}
	}

	public void WriteResultsToFile(string[] results,string report_safer,string score){
		if (User != "" && User != null) {
			StreamWriter writer = new StreamWriter (@"./Users/" + User + ".txt", true);
			writer.WriteLine ("Date:" + System.DateTime.Now.ToString ());
			writer.WriteLine ("Score:" + score);
			foreach (string s in results) {
				writer.WriteLine ("  -------------------------------------------------------------------");
				writer.WriteLine (s);
			}
			//writer.WriteLine ("  -------------------------------------------------------------------");
			string[] buffer = report_safer.Split ('\n');
			foreach (string s in buffer) {
				writer.WriteLine (string.Format ("  |   {0,-61} |", s));
			}

			writer.WriteLine ("  -------------------------------------------------------------------");
			writer.WriteLine ("***************************************************************************");
			writer.Close ();
		}
	}
}
