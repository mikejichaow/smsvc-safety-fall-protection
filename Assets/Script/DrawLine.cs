﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour {
	private LineRenderer _Linerender;
	public float counter;
	private float dist;

	public Transform origin;
	public Transform destination;

	public float LineDrawSpeed=6f;


	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () 
	{
		_Linerender=GetComponent<LineRenderer>();
		_Linerender.SetPosition(0, origin.position);
		_Linerender.SetWidth (0.08f, 0.08f);
		
		dist = Vector3.Distance (origin.position, destination.position);
		//if (counter < dist) 
		//{
			//counter+=0.1f/LineDrawSpeed;
		//if()
			float x=Mathf.Lerp(0,dist,1);

			Vector3 pointA=origin.position;
			Vector3 pointB=destination.position;

			Vector3 pointALongline=x*Vector3.Normalize(pointB-pointA)+pointA;

			_Linerender.SetPosition(1,pointALongline);
		//}
		//else
			//counter=0;

	}
}
