﻿using UnityEngine;
using System.Collections;

public class TestBack: MonoBehaviour {
    
    public bool gateBack;
    public GameObject testEnter;

	// Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        gateBack = true;
        testEnter.SetActive(true);
        testEnter.GetComponent<TestEnter>().gateEntered = false;
    }

	
}
