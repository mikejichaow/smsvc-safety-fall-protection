﻿using UnityEngine;
using System.Collections;

public class LockBaseLookat : MonoBehaviour {
	public GameObject Lockbase;
	private Vector3 _target;
	public GameObject Hook;
	// Use this for initialization
	void Start () {
		_target =Hook.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(_target);
	}
}
