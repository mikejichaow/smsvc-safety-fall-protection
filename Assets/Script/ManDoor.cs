﻿using UnityEngine;
using System.Collections;

public class ManDoor : MonoBehaviour {
public GameObject manDoor;
	public bool ClickOnDoor;

	public void OpenDoor(){
		manDoor.transform.localRotation = Quaternion.Euler (270, 180, 0);
	}

	public void CloseClickonDoor()
	{
		ClickOnDoor = false;
	}

	void Update () {
		if (GameObject.Find ("Scriptholder").GetComponent<Runtime> ().getRaycastController () == true) {
			if (Input.GetMouseButtonDown (0)) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
			
				//Debug.Log("a= " + a);
				if (Physics.Raycast (ray, out hit, 1000)) {

				
					if (hit.transform.tag == "HouseDoor") {
						ClickOnDoor = true;
						//manDoor.tag="";
						//manDoor.transform.localRotation = Quaternion.Euler (270, 180, 0);
					}
				}
			}
		}
	}

}
