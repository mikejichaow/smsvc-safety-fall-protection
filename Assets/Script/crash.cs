﻿using UnityEngine;
using System.Collections;

public class crash : MonoBehaviour {
    public GameObject CrashPanel;
    public bool crashed;
	public GameObject Resume;
	public GameObject Restart;

    void Start() {
        crashed = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider other) {
        CrashPanel.SetActive(true);
		if (Login.isInstructor == true) {
			Resume.SetActive (true);
			Restart.SetActive (false);
			GameObject.Find ("Scriptholder").GetComponent<Runtime>().deathcount++;
		} else {
			Resume.SetActive (false);
			Restart.SetActive (true);
		}
        crashed = true;
    }
}
