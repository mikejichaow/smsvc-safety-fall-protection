﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class RopeScale : MonoBehaviour {
	public GameObject _Player;
	public float speed = 1.5f;
	private Vector3 _target1;
	private Vector3 _target2;
	private Vector3 _target3;
	private Vector3 _target4;

	private Vector3 _target5;
	private Vector3 _target6;
	private Vector3 _target7;

	public GameObject Rhook;
	public GameObject Rhook2;
	public GameObject Hook1;
	public GameObject Hook2;
	public GameObject HookfPosition;
	public GameObject Hookf2Position;
	public float Dist1;
	public float Dist2;
	public GameObject Lanyard1;
	public GameObject Lanyard2;
	public enum ropeState {back, onTarget};
	public GameObject InstructionPanel;
	public Text InstructionText;
	public bool Dangerous;
	public GameObject Beamclaplight;	

	void Start () {
		//Rhook.SetActive (false);

	}
//	void Update() {
//		if (Input.GetMouseButtonDown(0)) {
//			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//			target.x = transform.position.x;
//		}
//		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
//	}

	// Update is called once per frame

		void Update () 
		{

		_target1 =Hook1.transform.position;
		_target2 =Hook2.transform.position;
		_target3 =Lanyard1.transform.position;
		_target4 =HookfPosition.transform.position;
		_target5 = Rhook.transform.position;
		_target6 = Hookf2Position.transform.position;
		_target7 = Rhook2.transform.position;
		//transform.TransformPoint (Vector3).zero;
		//Dist = Vector3.Distance (transform.position,Hook.transform.position);
		Dist1 = Vector3.Distance (Lanyard1.transform.position,Hook1.transform.position);
		Dist2 = Vector3.Distance (Lanyard2.transform.position,Hook2.transform.position);
		//Debug.Log ("Trans Position" + transform.position);
		//Debug.Log ("Hook's Position" + Hook.transform.position);
		//Debug.Log ("Dist1= " + Dist1);
		//Debug.Log ("Dist2= " + Dist2);
		//==========================================================Hook1========================================================================================
		//if (!Hooktest.Hook1trigger&&InventorySystem.attach1) {
			//Rhook.SetActive (true);
			Lanyard1.transform.localScale += new Vector3 (0f, 0f, 1.8f*Time.deltaTime);
			Rhook.transform.position=Vector3.MoveTowards(Rhook.transform.position,_target4,5.1f*Time.deltaTime);
			Lanyard1.transform.LookAt(_target5);
			//Rhook.transform.localScale-=new Vector3(0f,0f,1.8f*Time.deltaTime);
			//transform.localScale=new Vector3(transform.localScale.x,transform.localScale.y,Dist*0.06f);
		//} 
		//else if(Hooktest.Hook1trigger&&InventorySystem.attach1)
		{
			Lanyard1.transform.localScale=new Vector3(Lanyard1.transform.localScale.x,Lanyard1.transform.localScale.y,Dist1*0.45f);
		}
		//if (InventorySystem.attach1&&Dist1>=16.5f) 
		//{

			InstructionPanel.SetActive (true);
			InstructionText.text="You cannot move further before finding another anchor. Use the Beamclamp.";
			Beamclaplight.SetActive(true);
			//GameObject.Find("Player").GetComponent<FirstPersonController>().enabled = false;
		//}

		//if (InventorySystem.release1) 
		//{
			Lanyard1.transform.localScale = new Vector3 (0.09335271f, 0.2643293f, 0.104f);
		//}

		//==========================================================Hook2========================================================================================
		//if (!Hooktest.Hook2trigger&&InventorySystem.attach2) {
			Rhook2.SetActive(true);
			Lanyard2.transform.LookAt(_target6);
			Lanyard2.transform.localScale += new Vector3 (0f, 0f, 1.8f * Time.deltaTime);
			Rhook2.transform.position=Vector3.MoveTowards(Rhook2.transform.position,_target6,4.8f*Time.deltaTime);
			Lanyard2.transform.LookAt(_target7);
			//transform.localScale=new Vector3(transform.localScale.x,transform.localScale.y,Dist*0.06f);
		//} 
		//else if(InventorySystem.attach2)
		//{
			Lanyard2.transform.localScale=new Vector3(Lanyard2.transform.localScale.x,Lanyard2.transform.localScale.y,Dist2*0.3f);
		//}
		//if (InventorySystem.attach2&&Dist2>=19.5f) 
		//{
			
			InstructionPanel.SetActive (true);
			InstructionText.text="You cannot move further. You can only work in this area";
			Beamclaplight.SetActive(true);
			//GameObject.Find("Player").GetComponent<FirstPersonController>().enabled = false;
		//}
		
		//if (InventorySystem.release2) 
		//{
			Lanyard2.transform.localScale = new Vector3 (0.09335271f, 0.2643293f, 0.104f);
		//}


//			if (Input.GetKey (KeyCode.T))
//				{
//					transform.localScale += new Vector3 (0f, 0, 0.05f);
//				}
//
//			if (Input.GetKey (KeyCode.G))
//				{
//					transform.localScale -= new Vector3 (0f, 0, 0.05f);
//				}
//			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//			float hitdist = 10.0f;
//			Vector3 targetPoint = ray.GetPoint(hitdist);
//			Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
//			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);

		Lanyard1.transform.LookAt(_target1);
		Lanyard2.transform.LookAt(_target2);
		//Rhook.transform.LookAt (_target);
	//======================================================================================================================
		//Vector3 targetpoint2= 

		}

}
