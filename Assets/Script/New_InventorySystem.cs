﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;



public class New_InventorySystem : MonoBehaviour {
	public const int TOTAL =12;
	public GameObject[] Tool;
	public Sprite[] ItemImage;
	public Sprite[] ItemImage_Inspect;
	public Sprite[] ItemImage_Label;
	public GameObject[] slots = new GameObject[TOTAL];
	public GameObject[] slots1 = new GameObject[TOTAL];
	public GameObject SlotPanel;
	public string[] toolmane;
	public GameObject PickupPanel;
	public GameObject ToolImage;
	public GameObject ToolImage_Inspect;
	public GameObject ToolImage_Label;

	public GameObject Panel_Message;
	public Text Text_Message;
	public GameObject Button_Message;

	public GameObject Instructor1;
	public GameObject Instructor2;

	public GameObject FadeImage;
	//=====================================================Harness part==============================================================
	public GameObject[] Harness;
	public Sprite[] HarnessItemImage;
	public Sprite[] HarnessImage_Inspect;
	public GameObject HarnessPickPanel;
	public GameObject HarnessImage;
	public GameObject HarnessItemImage_Inspect;
	//public GameObject
	public static bool checkHarnessB1;
	public static bool checkHarnessB2;
	public static bool checkHarness;
    public bool[] checkPPEs;

	public GameObject _Harness;
	public GameObject _Helmet;
	public GameObject earPlug;
    public GameObject safetyGoggle;
	public GameObject glasses;

	public GameObject _Harness2;
	public GameObject _Helmet2;
	public int HarnessNumber;

    public GameObject BodyWithGloves;
    public Texture WithGloves;

	public GameObject Scriptholder;
	//==============================================================================================================================


	private RaycastHit Hit;
	public int currentI;
	public int c = 0;
	public List<bool> slotList;
	public string[] Label;
	
	public bool eyeCheckB;
	public bool earCheckB;
	public static bool checkBeamclamp;
	public static bool checkBeanstrap;
	public static bool checkLifeline;
	public static bool checkUsedLanyard;
	public static bool checkS_Lifeline;
	public static bool checkS_Used_Lifeline;


	
	public Text ToolLabel;
	public Text ToolName;
	public GameObject _Textlabel;

	public GameObject Lanyard1;
	public GameObject Lanyard2;
	public GameObject Book;
	public GameObject BookPanel;
    public GameObject panel_harnessFit;

    public GameObject sound_cheers;
    public GameObject sound_boom;
    public GameObject sound_cheers1;
    public GameObject sound_boom1;

	public GameObject Button_Inspect_Harness;
	public GameObject Button_Inspect_Tool;
	public GameObject Button_Report_Harness;
	public GameObject Button_Report_Tool;


	public bool[] InspectHarness;
	public bool[] InspectTools;
	public bool[] ReportHarness;
	public bool[] ReportTools;

	public bool B_ButtonInspectHarness;
	public bool B_ButtonReportHarness;


	public bool B_ButtonInspectTool;
	public bool B_ButtonReportTool;
	public bool BringRadio;
	public bool GrabBuddy;

	public GameObject ReportToSupervisor_Panel;
	public GameObject ReportToSupervisor_Text;

	public GameObject Report_panel_ReportTOSupervisor;
	public GameObject Report_panel_ReportTOSupervisor_result;

    //	bool msgB;
    //	float msgTimer;
    //	string msgText;
    //	
    //public InventorySlot slot;


    void Start(){
		InspectHarness=new bool[8];
		ReportHarness=new bool[8];
		InspectTools = new bool[9];
		ReportTools=new bool[9];
	}
	
	public void PickUpItems(){

		Scriptholder.GetComponent<Runtime>().step2++;
		if (Runtime.LearningMode){
			sound_cheers1.GetComponent<AudioSource>().Play();
		}
		
		_Textlabel.SetActive (false);
		if (slotList.Contains (false)) {
			c = slotList.IndexOf(false);
		}else{
			c = slotList.Count;
			slotList.Add (true);

		} 
		//ItemImage [c].SetActive (true);
		slots[c].GetComponent<Image>().sprite = ItemImage[currentI];
		slots [c].SetActive (true);
		Tool [currentI].SetActive (false);
		slotList [c] = true;
	}

	public void CheckLabel()
	{
		//ToolLabel.text=Label[1];
		_Textlabel.SetActive (true);

	}


	
	public void DropTool(int k){
		_Textlabel.SetActive (false);
		for (int x=0; x<TOTAL; x++) {
			if (slots[k].GetComponent<Image>().sprite == ItemImage[x])
			{
				Tool[x].SetActive(true);
				slots[k].GetComponent<Image>().sprite = null;
				slots[k].SetActive(false);
				slotList[k] = false;
				if (x==8){
					BringRadio=false;
				}
				
				if (x == 9)
				{
					earCheckB = false;
				}
				if (x == 10)
				{
					eyeCheckB = false;
				}
			}
		}
		
		slots[k].GetComponent<Image>().color = Color.white;
	}
	
	public void UseTool(int n) {
		_Textlabel.SetActive (false);
		gameObject.GetComponent<Runtime> ().RaycastControl (true);
		//USe Beamclamp
		if (slots[n].GetComponent<Image>().sprite == ItemImage[0]) 
		{
			checkBeamclamp=true;
			if (Runtime.LearningMode){
				sound_cheers1.GetComponent<AudioSource>().Play();
			}
        }

		//Use BeamStrap
		if (slots[n].GetComponent<Image>().sprite == ItemImage[1]) 
		{
			checkBeanstrap=true;
			if (Runtime.LearningMode){
				sound_cheers1.GetComponent<AudioSource>().Play();
			}
        }

		//Use Before_D_Selfline
		if (slots[n].GetComponent<Image>().sprite == ItemImage[2]) 
		{
			checkLifeline=true;
			Lanyard1.SetActive(true);
			Lanyard2.SetActive(true);
			GetComponent<Runtime>().goodLanyardB = true;
			GetComponent<Runtime>().LanyardUsed=true;
			if (Runtime.LearningMode){
				sound_cheers1.GetComponent<AudioSource>().Play();
			}
        }

		//Use Used_D_Selfline
		if (slots[n].GetComponent<Image>().sprite == ItemImage[4]) 
		{
			//checkBeanstrap=true;
			checkUsedLanyard=true;
			Lanyard1.SetActive(true);
			Lanyard2.SetActive(true);
            GetComponent<Runtime>().goodLanyardB = false;
			GetComponent<Runtime>().LanyardUsed=true;
			if (Runtime.LearningMode){
				sound_boom1.GetComponent<AudioSource>().Play();
			}
        }

		//Use Before_S_Lifeline
		if (slots[n].GetComponent<Image>().sprite == ItemImage[5]) 
		{			
			GetComponent<Runtime>().goodLanyardB = true;
			GetComponent<Runtime>().LanyardUsed=true;
			checkS_Lifeline=true;
			Lanyard1.SetActive(true);
			if (Runtime.LearningMode){
				sound_cheers1.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().score-=5;
        }

		//Use Used_S_Lifeline
		if (slots[n].GetComponent<Image>().sprite == ItemImage[6]) 
		{
			Lanyard1.SetActive(true);
			checkS_Used_Lifeline=true;
            GetComponent<Runtime>().goodLanyardB = false;
			GetComponent<Runtime>().LanyardUsed=true;
			if (Runtime.LearningMode){
				sound_boom1.GetComponent<AudioSource>().Play();
			}
        }
		if (slots1 [n].GetComponent<Image> ().sprite == ItemImage [8]&&GrabBuddy==true) {
			gameObject.GetComponent<Runtime>().CallForRescue();
		}

//		if (Tool[0].name=="1_BeamStrap") 
//		{
//			Debug.Log("Now you are using BeamStrap");
//		}
//
//
//		if (Tool[0].name=="2_Self_lifelineLanyardHook") 
//		{
//			Debug.Log("Now you are using Self_lifelineLanyardHook");
//		}






//
//		if (Tool[n].name=="1_BeamStrap") 
//		{
//			Debug.Log("Now you are using Tool2");
//		}
//
//		if (Tool[n].name=="2_Self_lifelineLanyardHook") 
//		{
//			Debug.Log("Now you are using Tool3");
//		}

		
	}

    public void wearHarness()
    {
        Debug.Log("Harness Number is: " + HarnessNumber);
		if (Scriptholder.GetComponent<Runtime>().step1>=3){
			Scriptholder.GetComponent<Runtime>().step1++;
		}

        if (HarnessNumber == 0)
        {
            //checkHarness=true;
            _Harness.SetActive(true);
            _Harness2.SetActive(true);
            Harness[0].SetActive(false);
			if (!Runtime.LearningMode){
			Harness[1].SetActive(true);
			Harness[2].SetActive(true);
			}
            panel_harnessFit.SetActive(true);
            GetComponent<Runtime>().goodHarnessB = true;
			if (Runtime.LearningMode){
				sound_cheers.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[1]=true;
        }

        if (HarnessNumber == 1)
        {
            //checkHarnessB1=true;
            _Harness.SetActive(true);
            _Harness2.SetActive(true);
            Harness[1].SetActive(false);
			Harness[0].SetActive(true);
			Harness[2].SetActive(true);
            panel_harnessFit.SetActive(true);
            GetComponent<Runtime>().goodHarnessB = false;
			if (Runtime.LearningMode){
				sound_boom.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[1]=true;
        }
        if (HarnessNumber == 2)
        {
            //checkHarnessB2=true;
            _Harness.SetActive(true);
            _Harness2.SetActive(true);
            Harness[2].SetActive(false);
			Harness[1].SetActive(true);
			Harness[0].SetActive(true);
            panel_harnessFit.SetActive(true);
            GetComponent<Runtime>().goodHarnessB = false;
			if (Runtime.LearningMode){
				sound_boom.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[1]=true;
        }
        if (HarnessNumber == 3)
        {
            _Helmet.SetActive(true);
            _Helmet2.SetActive(true);
            Harness[3].SetActive(false);
			if (!Runtime.LearningMode){
				Harness[6].SetActive(true);
			}else{
				sound_cheers.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[0]=true;
			GetComponent<Runtime>().goodHelmetB = true;
        }
        if (HarnessNumber == 4)
        {
            safetyGoggle.SetActive(true);
            Harness[4].SetActive(false);
			glasses.GetComponent<Animation>().Play();
			if (Runtime.LearningMode){
				sound_cheers.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[4]=true;
        }
        if (HarnessNumber == 5)
        {
            earPlug.SetActive(true);
            Harness[5].SetActive(false);
			if (Runtime.LearningMode){
				sound_cheers.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[3]=true;
        }
        if (HarnessNumber == 6)
        {
            _Helmet.SetActive(true);
            _Helmet2.SetActive(true);
            Harness[6].SetActive(false);
			Harness[3].SetActive(true);
			if (Runtime.LearningMode){
				sound_boom.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[0]=true;
			GetComponent<Runtime>().goodHelmetB = false;
        }
        if (HarnessNumber == 7)
        {
            Harness[7].SetActive(false);
            BodyWithGloves.GetComponent<Renderer>().materials[1].mainTexture = WithGloves;
			if (Runtime.LearningMode){
				sound_cheers.GetComponent<AudioSource>().Play();
			}
			gameObject.GetComponent<Runtime>().ppes[2]=true;
        }

        checkPPEs[HarnessNumber] = true;
    }
	

	public void SetActive (GameObject g)
	{
		if (g.activeSelf == true) {
			g.SetActive (false);
		} else {
			g.SetActive (true);
		}
	}

	public void ClickOnHarness_Inspect()
	{
		Debug.Log ("Has Inspect the Harness NO." + HarnessNumber);
				InspectHarness[HarnessNumber]=true;
				
				if(B_ButtonReportHarness)
				{
					ReportHarness[HarnessNumber]=true;
					
				}
				else 
					ReportHarness[HarnessNumber]=false;
	
	}

	public void ClickOnHarness_Report()
	{
		Report_panel_ReportTOSupervisor.SetActive (true);
		Report_panel_ReportTOSupervisor_result.SetActive (true);

		ReportHarness[HarnessNumber]=true;
		B_ButtonReportHarness = true;
		Debug.Log ("Has Reported the harness NO." + HarnessNumber);

		if (HarnessNumber == 1 || HarnessNumber == 2 || HarnessNumber == 6) 
		{
			ReportToSupervisor_Text.GetComponent<Text>().text="You follow your company's procedure of damaged equipment. Please choose another one.";
		}
		if (HarnessNumber == 0 || HarnessNumber == 3 || HarnessNumber == 4 || HarnessNumber == 5||HarnessNumber==7) 
		{
			ReportToSupervisor_Text.GetComponent<Text>().text="You report to supervisor, but he said it's OK.";
		}
	}

	public void ClickOnTool_Inspect()
	{
		Debug.Log ("Has Inspect the Tool NO." + currentI);
		InspectTools [currentI] = true;
		if (B_ButtonReportTool) {
			ReportTools [currentI] = true;
		} else
			ReportTools [currentI] = false;
	}

	public void ClickOnTool_Report()
	{
		Report_panel_ReportTOSupervisor.SetActive (true);
		Report_panel_ReportTOSupervisor_result.SetActive (true);

		if (currentI==4||currentI==6) 
		{
			ReportToSupervisor_Text.GetComponent<Text>().text="You follow your company's procedure of damaged equipment. Please choose another one.";
		}
		if (currentI==0||currentI==1||currentI==2||currentI==3||currentI==5||currentI==8) 
		{
			ReportToSupervisor_Text.GetComponent<Text>().text="You report to supervisor, but he said it's OK.";
		}


		ReportTools [currentI] = true;
		B_ButtonReportTool = true;
		Debug.Log ("Has Reported to tool NO." + currentI);
	}

	public void CopySlots(){
		for (int i=0; i<TOTAL; i++) {
			if ( slots [i].GetComponent<Image> ().sprite!=null)
			{
				slots1 [i].GetComponent<Image> ().sprite = slots [i].GetComponent<Image> ().sprite;
				slots1 [i].SetActive (true);
			}
		}
	}

	public void GetAccompany(){
		Panel_Message.SetActive (true);
		Text_Message.text="I will be at the work area waiting for you.";
		GrabBuddy=true;
		Instructor1.SetActive (false);
		Instructor2.SetActive (true);
	}

	public void RaycastControl(){
		gameObject.GetComponent<Runtime>().RaycastControl(!SlotPanel.activeSelf);
	}

	void Update () {
		if (gameObject.GetComponent<Runtime>().getRaycastController()==true){
			if(Input.GetMouseButtonDown(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray,out hit, 1000)) 
				{
					Debug.Log("tool: "+hit.collider.name);
					if(hit.transform.tag == "Book")
					{
						Debug.Log("Book");
						Book.SetActive(false);
						BookPanel.SetActive(true);
						gameObject.GetComponent<Runtime>().setRigidBodyFPSController(false);
					}
					if(hit.transform.tag == "Tool" && !PickupPanel.active)
					{
						Runtime.Actionarray.Add ("123");
						Runtime.Actionarray.Add ("456");
						//Debug.Log ("Actionarry is: "+ Actionarray.ToString());
						//Runtime.Actionarray.Add("Click on tool");

						string[] toolName = hit.collider.name.Split('_');
						int i = int.Parse(toolName[0]);
						PickupPanel.SetActive(true);
						ToolImage.GetComponent<Image> ().sprite = ItemImage[i];
						ToolImage_Inspect.GetComponent<Image>().sprite=ItemImage_Inspect[i];
						if (i==8){
							//Button_Inspect_Tool.SetActive(false);
							ToolImage_Label.SetActive(false);
							BringRadio=true;
						}
						else{
							Button_Inspect_Tool.SetActive(true);
							ToolImage_Label.SetActive(true);
						}
						ToolImage_Label.GetComponent<Image>().sprite=ItemImage_Label[i];
						ToolLabel.text=Label[i];
						ToolName.text=toolmane[i];
						currentI = i;

						FadeImage.SetActive(false);
					}

					if (hit.transform.name=="Instructor1")
					{
						Panel_Message.SetActive(true);
						Button_Message.SetActive(true);
						gameObject.GetComponent<Runtime>().RaycastControl (false);
						Text_Message.text="Do you wan to bring a buddy to accompany you?";
					}

					if (hit.transform.name=="Instructor2")
					{
						Panel_Message.SetActive(true);
						Button_Message.SetActive(false);
						Text_Message.text="I will be right here with you.";
					}

					if(hit.transform.tag=="Harness"&&!HarnessPickPanel.active)
					{
						//Runtime.Actionarray.Add("Click on harness");
						string[] HarnessName=hit.collider.name.Split('_');
						int i =int.Parse(HarnessName[0]);

						HarnessNumber=i;
						HarnessPickPanel.SetActive(true);
						HarnessImage.GetComponent<Image>().sprite=HarnessItemImage[i]; 
						HarnessItemImage_Inspect.GetComponent<Image>().sprite=HarnessImage_Inspect[i]; 
					}

					if (hit.transform.name == "OrderBook" && !BookPanel.active)
					{
						Book.SetActive(false);
						BookPanel.SetActive(true);
					}
				}
			}
		}
	}
}