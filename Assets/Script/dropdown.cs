﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class dropdown : MonoBehaviour,IPointerExitHandler,IPointerEnterHandler {
    public RectTransform container;
    public bool isOpen;

	// Use this for initialization
	void Start () {
        container = transform.FindChild("dropsdown").GetComponent<RectTransform>();
        isOpen = false;
	}

    public void OpenMenu()
    {
        isOpen = true;
    }

    public void CloseMenu()
    {
        isOpen = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (isOpen)
        {
            Vector3 scale = container.localScale;
            scale.y = Mathf.Lerp(scale.y, 1 , Time.deltaTime * 20);
            container.localScale = scale;
        }
        else
        {
            Vector3 scale = container.localScale;
            scale.y = Mathf.Lerp(scale.y, 0, Time.deltaTime * 20);
            container.localScale = scale;
        }
	}



    public void OnPointerExit(PointerEventData eventData)
    {
        isOpen = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOpen = true;
    }
}
