﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorkAreaTest : MonoBehaviour {

	public GameObject WorkPanel;
	public Text TimerText;
	public float Timer=10f;
	public static bool timerstart;
	public GameObject Workareasign;
	public GameObject Doortest;//After finish work, close the old test and open the new door test
	public GameObject Doortestfinish;
    public GameObject DoortestBack;
	public GameObject QuitButton;
	public GameObject SummaryButton;

    public GameObject scriptholder;

	// Use this for initialization
	void Start () {
	
	}

	void Update () 
	{
		if (timerstart) 
		{
			if (Timer >= 0f) 
			{
				Timer -= Time.deltaTime;
			
				TimerText.text = Timer.ToString("f0");

			} 
			else 
			{
				TimerText.text="The work is finished. \nPlease go back to ground.";
				timerstart=false;
				QuitButton.SetActive(true);
				SummaryButton.SetActive(true);
				Workareasign.SetActive(false);
				Doortest.SetActive(false);
				Doortestfinish.SetActive(true);
                DoortestBack.SetActive(true);
			}

		}
	}
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
	{
		timerstart = true;
		WorkPanel.SetActive (true);
        scriptholder.GetComponent<Runtime>().step3 = 8;



    }
}
