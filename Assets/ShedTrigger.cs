﻿using UnityEngine;
using System.Collections;

public class ShedTrigger : MonoBehaviour {

	public GameObject ClearnanceMessage;
	public static bool isToggle=true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter (Collider other)
	{
		if (isToggle) {
			ClearnanceMessage.SetActive (true);
			gameObject.SetActive (false);
			GameObject.Find ("Scriptholder").GetComponent<Runtime> ().RaycastControl (false);
		}
	}
}
