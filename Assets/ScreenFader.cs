﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour {

	public float fadeSpeed = 0.5f; // Speed that the screen fades to and from black.
	
	private bool sceneStarting = true; // Whether or not the scene is still fading in.
	


	public GameObject FadeImage;
	
	void Awake ()
		
	{
		

		
	}
	
	void Update ()
		
	{
		

		FadeImage.GetComponent<Image> ().color = Color.Lerp (GetComponent<Image> ().color, Color.clear, fadeSpeed * Time.deltaTime);

		if(FadeImage.GetComponent<Image> ().color.a <= 0.09f)
			
		{
			
			
			
			
			
			FadeImage.GetComponent<Image> ().enabled = false;
			Debug.Log("Fade effect close");
			FadeImage.SetActive(false);
			
			// The scene is no longer starting.
			
			sceneStarting = false;
			
		}
		
	}
	
	public void FadeToClear ()
		
	{

		FadeImage.GetComponent<Image> ().color = Color.Lerp (GetComponent<Image> ().color, Color.clear, fadeSpeed * Time.deltaTime);
		
	}
	
	public void FadeToBlack ()
		
	{
		

		FadeImage.GetComponent<Image> ().color = Color.Lerp (GetComponent<Image> ().color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	void StartScene ()
		
	{
			
		FadeToClear();

	
		

		
	}
	
	public void EndScene (string name)
		
	{
		
		// Make sure the texture is enabled.
		
		GetComponent<GUITexture>().enabled = true;
		
		// Start fading towards black.
		
		FadeToBlack();
		
		// If the screen is almost black…
		
		if(GetComponent<GUITexture>().color.a >= 0.95f)
			
			// … reload the level.
			
			Application.LoadLevel(name);
		
	}
	
}